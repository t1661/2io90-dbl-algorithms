package group5;

import java.util.Random;
import java.util.Scanner;

import AlgorithmSlider1.AlgoSliderOne;
import DataStructure.LabelsContainer;
import DataStructure.Placement;

public class SliderTest {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        boolean gui = false;
        Graphics.Display display = null;

        if (gui) {
            display = new Graphics.Display();
            Thread t = new Thread(display);
            t.start();
        }

        double n = 10;
        do {
            LabelsContainer lc = ReadInput.loadRandom((int) n, Placement.Slider, (float) (Math.pow(Math.E, rand.nextGaussian())), 1.0f);
            AlgoSliderOne solver = new AlgoSliderOne(lc);
            AlgoSliderOne.show = false;

            long startnano = System.nanoTime();
            solver.execute();
            long stopnano = System.nanoTime();

            if (gui) {
                display.setContent(lc);
                solver.show();
                display.autoFit();

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println((int) n + " labels, execution time: " + (stopnano - startnano)/1000000.0 + "ms, height: " + lc.getHeight() + ", aspect ratio: " + lc.ratio);

            n *= 1.01;
        } while (n < 10000);
    }
}
