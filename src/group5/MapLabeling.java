/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tomas
 */
package group5;

import DataStructure.*;
import java.io.FileNotFoundException;
import Algorithm1.AlgoOne;
import java.util.Scanner;
import java.util.Random;
import java.util.Collections;

public class MapLabeling {

    public static void main(String[] args) throws FileNotFoundException {

        Graphics.Display display = new Graphics.Display();
        Thread t = new Thread(display);
        t.start();

        try {
            Thread.sleep(3000);
        } catch (Exception e) {}
        AlgoOne.show = true;
        AlgoOne.testAlgo();

        LabelsContainer lc = null;
        //try {lc = ReadInput.loadInputFromFile("4pos10000");} catch (Exception e) {}
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                lc = ReadInput.loadInputFromFile("4pos10000");
            } catch (Exception e) {}
            //try {lc = ReadInput.loadInputFromFile("test00");} catch (Exception e) {}
            //lc = ReadInput.loadRandom(10, Placement.FourPos, 0.9f, 1.0f, true);

            //SatisFiability2Pos.SatGraphAlgo algo = new SatisFiability2Pos.SatGraphAlgo();
            AlgoOne algo = new AlgoOne(lc);

            long timeStart = System.currentTimeMillis();
            algo.execute();
            long totalTime = System.currentTimeMillis() - timeStart;

            lc.setAllIntersections();
            //lc.setHeight(lc.getHeight() * 10);
            Graphics.Display.setContent(lc);
            display.autoFit();

            System.out.print("Time: " + totalTime);
            sc.nextLine();
        }
    }

    private static void solve(LabelsContainer lc) {
        //try {lc = ReadInput.loadInputFromFile("4pos10000");} catch (Exception e) {}
        //lc = ReadInput.loadRandom(10000, Placement.FourPos, 1.472f, 2.5454f, true);

        AlgoOne runner = new AlgoOne(lc);
        //AlgoOne.show = true;

        long startTime = System.currentTimeMillis();
        runner.execute2();
        long endTime = System.currentTimeMillis() - startTime;

        lc.setAllIntersections();
        if (AlgoOne.lastFail != null) {
            AlgoOne.lastFail.hasIntersectForvisual = true;
        }

        System.out.println(lc.getHeight() + "  time : " + endTime);

        Graphics.Display.setContent(lc);
        Graphics.Display.autoFit();
    }

    public static void testFit() {
        LabelsContainer lc = null;
        //try {lc = ReadInput.loadInputFromFile("4pos10000");} catch (Exception e) {}
        Scanner sc = new Scanner(System.in);

        while (true) {
            lc = ReadInput.loadRandom(8, Placement.FourPos, 1.472f, 2.5454f, true);

            long startTime = System.currentTimeMillis();
            lc.snapDown();
            long endTime = System.currentTimeMillis() - startTime;

            System.out.println(lc.getHeight() + "  time : " + endTime);

            lc.setAllIntersections();
            Graphics.Display.setContent(lc);
            Graphics.Display.autoFit();
            sc.nextLine();
        }
    }

    public static void testShuffle() {
        LabelsContainer lc = null;
        float bestH = 0;
        for (int i = 50; i < 1000; i++) {
            //try {lc = ReadInput.loadInputFromFile("4pos10000");} catch (Exception e) {}
            lc = ReadInput.loadRandom(100, Placement.FourPos, 1, 20);

            Collections.shuffle(lc.labelList, new Random(i));

            AlgoOne runner = new AlgoOne(lc);
            AlgoOne.show = true;

            long startTime = System.currentTimeMillis();
            runner.execute();
            long endTime = System.currentTimeMillis() - startTime;

            lc.setAllIntersections();

            if (lc.getHeight() > bestH) {
                Graphics.Display.setContent(lc);
                Graphics.Display.autoFit();
                bestH = lc.getHeight();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
            }

            System.out.println(lc.getHeight() + " / " + bestH + "  time : " + endTime);
        }
    }

}