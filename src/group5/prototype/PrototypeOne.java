/**
 * This prototype has to read every input line and output it again.
 */

package group5.prototype;

import java.util.ArrayList;
import java.util.Scanner;

public class PrototypeOne {

    private String model;
    private float aspectRatio;
    private ArrayList<Label> labelList;

    /**
     * Initialize members
     */
    public PrototypeOne() {
        this.model = "";
        this.aspectRatio = 0.0f;
        this.labelList = new ArrayList<Label>();
    }

    /**
     * Read the input, then output the input (in the same format)
     */
    public void run() {
        readInput();
        outputInput();
    }

    /**
     * Read the input head followed by a number of points (may be zero)
     */
    private void readInput() {
        Scanner scanner = new Scanner(System.in);
        int numPoints = 0;
        int labelX, labelY;
        String temp;

        // read the placement model string
        temp = scanner.nextLine();
        this.model = temp.substring(17);

        // read the aspect ratio string as a float
        temp = scanner.nextLine();
        this.aspectRatio = Float.parseFloat(temp.substring(14));

        // read the number of points as an integer
        temp = scanner.nextLine();
        numPoints = Integer.parseInt(temp.substring(18));

        // read the number of points (two integers per point)
        for (int n = 0; n < numPoints; n += 1) {
            labelX = scanner.nextInt();
            labelY = scanner.nextInt();
            this.labelList.add(new Label(labelX, labelY));
        }
    }

    /**
     * Output the input in the same format
     */
    private void outputInput() {
        // output the input header
        System.out.println("placement model: " + this.model);
        System.out.println("aspect ratio: " + this.aspectRatio);
        System.out.println("number of points: " + this.labelList.size());

        // output each label's x and y coordinate
        for (Label label : this.labelList) {
            System.out.println(label.x + " " + label.y);
        }
    }

    /**
     * Initialize the program, create the prototype and run it
     * @param args
     */
    public static void main(String[] args) {
        // create and run the prototype
        new PrototypeOne().run();
    }
}
