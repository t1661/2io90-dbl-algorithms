package group5.datamodel;

public class LabelPoint extends Vector2 {
    // note: height/width = ratio => width = height * ratio
    private double  ratio;
    private Vector2 size;

    public enum Placement {
        TwoPos, FourPos, Slider
    }

    public enum Corner {
        NE, NW, SE, SW
    }

    private Placement   placement;
    private Corner      corner;

    public LabelPoint(int x, int y, double ratio) {
        super(x, y);
        this.ratio = ratio;
        this.size = new Vector2();
    }

    public void setHeight(double height) {
        size.x = ratio * height;
        size.y = height;
    }

    // TODO: test intersection via quad tree
    //       query the range of own point -/+ twice the size to get all possible labels
}
