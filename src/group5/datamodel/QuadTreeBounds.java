package group5.datamodel;

/**
 * AABB subclass to provide the quad tree with split bounds
 *
 * @author Jochem
 */
public class QuadTreeBounds extends AABB {

    public QuadTreeBounds(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public QuadTreeBounds(Vector2 center, Vector2 extent) {
        super(center, extent);
    }

    /**
     * Enumeration for the four different corners
     */
    public enum Quarter {
        NORTHEAST(-1, -1),
        NORTHWEST( 1, -1),
        SOUTHEAST(-1,  1),
        SOUTHWEST( 1,  1);

        /**
         * Horizontal splitting offset
         */
        private int horOffset;

        /**
         * Vertical splitting offset
         */
        private int verOffset;

        /**
         * Set enumeration members
         * @param horOffset horizontal splitting offset
         * @param verOffset vertical splitting offset
         */
        Quarter(int horOffset, int verOffset) {
            this.horOffset = horOffset;
            this.verOffset = verOffset;
        }

        /**
         * Get the bounding box corresponding to this corner
         * @param center
         * @param extent
         * @return bounding box
         */
        public QuadTreeBounds getQuadTreeBounds(Vector2 center, Vector2 extent) {
            return new QuadTreeBounds(
                new Vector2(
                    center.x + horOffset * extent.x / 2.0,
                    center.y + verOffset * extent.y / 2.0
                ),
                new Vector2(extent.x / 2.0, extent.y / 2.0));
        }
    };

    /**
     * Returns the bounding box of the corresponding quarter
     * @param quarter
     * @return bounding box
     */
    public QuadTreeBounds getQuarter(QuadTreeBounds.Quarter quarter) {
        return quarter.getQuadTreeBounds(center, extent);
    }
}
