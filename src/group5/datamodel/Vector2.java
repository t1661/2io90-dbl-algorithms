package group5.datamodel;

/**
 * 2D double vector class.
 * Has public members so x and y can be accessed directly for more readable code.
 *
 * @author Jochem
 */
public class Vector2 {
    /**
     * x component
     */
    public double x;

    /**
     * y component
     */
    public double y;

    /**
     * Creates a new 2D vector
     * @param x x component
     * @param y y component
     */
    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Creates a new 2D vector
     */
    public Vector2() {
        this(0.0, 0.0);
    }

    /**
     * toString method
     * @return string representation
     */
    public String toString() {
        return "(" + x + "," + y + ")";
    }
}
