package group5.datamodel;

/**
 * Axis Aligned Bounding Box class
 *
 * @author Jochem
 */
public class AABB {
    /**
     * Center point of the bounding box
     */
    protected Vector2 center;

    /**
     * Distance from the center point to the edge
     */
    protected Vector2 extent;

    /**
     * Create a bounding box from a center vector and an extent vector
     * @param center the center point
     * @param extent the distance from the center point to the edge
     */
    public AABB(Vector2 center, Vector2 extent) {
        this.center = center;
        this.extent = extent;
    }

    /**
     * Create a bounding box from the left top coordinate and a width and height
     * @param x left top x
     * @param y left top y
     * @param width width of the bounding box
     * @param height height of the bounding box
     */
    public AABB(double x, double y, double width, double height) {
        this(
            new Vector2(x + width/2.0, y + height/2.0),
            new Vector2(width/2.0, y + height/2.0)
        );
    }

    /**
     * Returns true if the point is within this bound
     * @param point
     * @return whether or not the point is within this bound
     */
    public boolean containsPoint(Vector2 point) {
        if (point == null) {
            return false;
        }
        return (point.x >= center.x - extent.x && point.x < center.x + extent.x
                && point.y >= center.y - extent.y && point.y < center.y + extent.y);
    }

    /**
     * Returns true if bounds is intersecting this bounding box
     * @param bounds
     * @return whether or not the bounds intersect or overlap (one inside the other)
     */
    public boolean intersects(AABB bounds) {
        return (Math.abs(center.x - bounds.getCenter().x) < extent.x + bounds.getExtent().x
                && Math.abs(center.y - bounds.getCenter().y) < extent.y + bounds.getExtent().y);
    }

    /**
     * @return center vector
     */
    public Vector2 getCenter() {
        return center;
    }

    /**
     * @return extend vector
     */
    public Vector2 getExtent() {
        return extent;
    }

    public String toString() {
        return "AABB((" + (center.x - extent.x) + "," + (center.y - extent.y) +
               ") to (" + (center.x + extent.x) + "," + (center.y + extent.y) + "))";
    }
}
