package group5.datamodel;

import java.util.ArrayList;

/**
 * QuadTree node class for storing QuadTreePoint objects
 *
 * <p>NB: The root element of a quad tree is also a node</p>
 *
 * @author Jochem
 */
public class QuadTree {
    /**
     * The maximum number of objects a node can contain before it splits.
     */
    static final int QT_NODE_CAPACITY = 4;

    /**
     * Bounds of this quad tree node
     */
    private QuadTreeBounds bounds;

    /**
     * An array of the points stored in this node
     */
    private Vector2[] points;

    /**
     * An array of the child nodes of this node
     */
    private QuadTree[] nodes;

    /**
     * Create a quad tree node
     * @param bounds bounding box
     */
    public QuadTree(QuadTreeBounds bounds) {
        this.bounds = bounds;
        this.points = new Vector2[QT_NODE_CAPACITY];

        // this will hold the four corner nodes
        this.nodes = new QuadTree[4];
    }

    /**
     * Insert a point in the quad tree
     * @param point the point to insert
     * @return true on success, false on failure (e.g. point outside bounding box)
     */
    public boolean insert(Vector2 point) {

        // return false if the point does not belong in this node
        if (!bounds.containsPoint(point)) {
            return false;
        }

        // if this node is a leaf and we can store the point, store it
        if (isLeaf()) {
            for (int i = 0; i < points.length; i += 1) {
                if (points[i] == null) {
                    points[i] = point;
                    return true;
                }
            }

            // apparently we could not place the point in our points array
            // so we need to split this node and divide points equally
            split();

            // now this node is no longer a leaf and the point can be added using
            // existing code
        }

        // if this node is not a leaf, try inserting it in any of the nodes and return on success
        for (int i = 0; i < 4; i += 1) {
            if (nodes[i].insert(point)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Find all points in the given range
     * @param range search range
     * @return an ArrayList of points
     */
    public ArrayList<Vector2> queryRange(AABB range) {
        ArrayList<Vector2> pointlist = new ArrayList<Vector2>();

        // find the points in this range, it's faster to pass the ArrayList by
        // reference because it doesn't have to merge each recursive step
        queryRange(range, pointlist);

        return pointlist;
    }

    /**
     * Find all points in the given range
     * @param range search range
     * @param pointlist ArrayList to place the points in
     */
    public void queryRange(AABB range, ArrayList<Vector2> pointlist) {

        // stop if the range doesn't intersect (or overlap) this bound
        if (!range.intersects(bounds)) {
            return;
        }

        // if this is a leaf, check every point and add them to the list
        if (isLeaf()) {
            for (int i = 0; i < points.length; i += 1) {
                if (range.containsPoint(points[i])) {
                    pointlist.add(points[i]);
                }
            }

        } else {
            // it's not a leaf; continue query on child nodes

            for (int n = 0; n < 4; n += 1) {
                nodes[n].queryRange(range, pointlist);
            }
        }
    }

    /**
     * Split the node in 4 child nodes
     */
    private void split() {
        // create four new nodes
        nodes[0] = new QuadTree(bounds.getQuarter(QuadTreeBounds.Quarter.NORTHEAST));
        nodes[1] = new QuadTree(bounds.getQuarter(QuadTreeBounds.Quarter.NORTHWEST));
        nodes[2] = new QuadTree(bounds.getQuarter(QuadTreeBounds.Quarter.SOUTHEAST));
        nodes[3] = new QuadTree(bounds.getQuarter(QuadTreeBounds.Quarter.SOUTHWEST));

        // divide the points over the new nodes
        for (int i = 0; i < points.length; i += 1) {

            // for each point try to insert it in one of the nodes
            for (int n = 0; n < 4; n += 1) {

                // when we successfully insert a point, continue with the next point
                if (nodes[n].insert(points[i])) {
                    continue;
                }
            }
        }
    }

    /**
     * Checks whether or not this node is a leaf
     * @return true if a leaf, false otherwise
     */
    private boolean isLeaf() {
        return (nodes[0] == null);
    }

    public String toString() {
        return bounds.toString();
    }
}
