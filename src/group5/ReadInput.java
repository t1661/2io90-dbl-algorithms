/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package group5;

import DataStructure.Label;
import DataStructure.LabelPosition;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import DataStructure.Placement;
import DataStructure.LabelsContainer;

import java.util.Random;

public class ReadInput {

    public static LabelsContainer loadInputFromFile() throws FileNotFoundException {
        System.out.println("Type Input File Name: ");
        Scanner sc = new Scanner(System.in);
        String fileName = sc.next();

        return loadInputFromFile(fileName);
    }
    public static LabelsContainer loadInputFromFile(String fileName) throws FileNotFoundException {
        Placement placeM = null;
        float aspect_ratio = -1;
        int nr_of_labels = -1;

        File input_file = new File("src/" + fileName + ".txt");
        Scanner sc = new Scanner(input_file);

        String temp = sc.nextLine();
        if (temp.contains("placement")) {
            temp = temp.substring(17);
            switch (temp) {
            case "1slider":
                placeM = Placement.Slider;
                break;
            case "2pos":
                placeM = Placement.TwoPos;
                break;
            case "4pos":
                placeM = Placement.FourPos;
                break;
            }
        } else {
            System.out.println("Incorrect File or File may be incorrectly formatted.");
            System.exit(0);
        }



        temp = sc.nextLine();
        if (temp.contains("aspect")) {
            temp = temp.substring(13);
            aspect_ratio = Float.parseFloat(temp);
        } else {
            System.out.println("Incorrect File or File may be incorrectly formatted.");
            System.exit(0);
        }

        temp = sc.nextLine();
        if (temp.contains("number") && temp.contains("points")) {
            temp = temp.substring(18);
            nr_of_labels = Integer.parseInt(temp);
        } else {
            System.out.println("Incorrect File or File may be incorrectly formatted.");
            System.exit(0);
        }

        //there has beeen a load error if :
        if (aspect_ratio == -1 || nr_of_labels == -1) {
            return null;
        }

        //no loading error (yet) create the label with the values loaded
        LabelsContainer lc = new LabelsContainer(placeM, aspect_ratio);

        for (int i = 0; i < nr_of_labels; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();

            lc.addLabel(x,y);
        }

        sc.close();

        lc.allLabelsAdded();

        return lc;
    }
    public static LabelsContainer loadRandom(int pointCount, Placement p, float aspectRatio, float initialVheight) {
        return loadRandom(pointCount,p,aspectRatio,initialVheight,false);
    }
    public static LabelsContainer loadRandom(int pointCount, Placement p, float aspectRatio, float initialVheight, boolean randomizePositions) {
        LabelsContainer lc = new LabelsContainer(p, aspectRatio);
        lc.setHeight(initialVheight);

        Random r = new Random();
        r.setSeed(System.currentTimeMillis());

        int fieldSize = (int) (3.0f*pointCount);

        int placedPoints = 0;
        for(int i = 0; i < pointCount * 2; i++) {
            int xid = (int) (r.nextDouble() * fieldSize);
            int yid = (int) (r.nextDouble() * fieldSize);
            if (!lc.hasLabelAt(xid, yid)) {
                lc.addLabel(xid,yid);
                placedPoints  += 1;
                if (placedPoints == pointCount) break;
            }
        }

        if (randomizePositions) {
            LabelPosition[] positions = LabelPosition.values();
            for(Label l : lc.labelList) {
                l.setPosition(positions[r.nextInt(4)]);
            }
        }

        lc.allLabelsAdded();

        return lc;
    }

}
