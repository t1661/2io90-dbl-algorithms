/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satisfiabilty4Pos;

import DataStructure.*;
import java.util.ArrayList;

/**
 *
 * @author Bart
 */
public class LabelExt4Pos {

    Label TR;
    boolean TRalive = true;
    Label TL;
    boolean TLalive = true;
    //Label BR;
    //boolean BRalive = true;
    //Label BL;
    //boolean BLalive = true;

    public LabelExt4Pos(Label l) {
        TR = l;
        TR.setPosition(LabelPosition.TopRight);
        TL = l.simpleCopy(0, true);
        //BL = l.simpleCopy(0, false);
        //BR = l.simpleCopy(1, false);
    }
    
    public void close()
    {
        TL.close();
        //BR.close();
        //BL.close();
    }
    
    public void setDead()
    {
        if (TR.overlapsAlabelBase()) TRalive = false;
        if (TL.overlapsAlabelBase()) TLalive = false;
        //if (BR.overlapsAlabelBase()) BRalive = false;
        //if (BL.overlapsAlabelBase()) BLalive = false;
    }
    
    public void setPossibility()
    {
        if (TR.overlapsAlabelBase())
        {
            Algorithm4Pos.ExcludedMiddle middle = (Algorithm4Pos.ExcludedMiddle) TR.Attributes.get(Algorithm4Pos.ExcludedMiddle.class);
            
        }
        if (TL.overlapsAlabelBase())
        {
            Algorithm4Pos.ExcludedMiddle middle = (Algorithm4Pos.ExcludedMiddle) TL.Attributes.get(Algorithm4Pos.ExcludedMiddle.class);
            
        }
    }


}
