/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satisfiabilty4Pos;

import java.util.ArrayList;
import DataStructure.*;

/**
 *
 * @author Bart
 */
public class CreateExpressions {

    public static void create(ArrayList<Expression> expressions, LabelsContainer lc) {
        // do a sweep line fo find the intersections
        ArrayList<Label> doneLabels = new ArrayList();
        for (Label l : lc.labelList) {
            ArrayList<Label> iList = l.intersectList();
            for (Label l2 : iList) {
                if (!l.samePos(l2) && !doneLabels.contains(l2)) {
                    if (l.getShift() == l2.getShift()) {
                        if (l.xBase == l2.xBase) {
                            if (l.getShift() == 0) {
                                expressions.add(new Expression(l, true, l2, true));
                            } else {
                                expressions.add(new Expression(l, false, l2, false));
                            }
                        } 
                        else if (l.getShift() == 1)
                        {
                            if (l.xBase < l2.xBase)
                            {
                                expressions.add(new Expression(l, false, l, false));
                            }
                            else {
                                expressions.add(new Expression(l2, false, l2, false));
                            }
                        }
                        else {
                            if (l.xBase < l2.xBase)
                            {
                                expressions.add(new Expression(l2, true, l2, true));
                            }
                            else {
                                expressions.add(new Expression(l, true, l, true));
                            }
                        }
                    } else if (l.xBase > l2.xBase) {
                        expressions.add(new Expression(l, true, l2, false));
                    } else {
                        expressions.add(new Expression(l, false, l2, true));
                    }
                }
            }
            doneLabels.add(l);
        }

        for (int i = 0; i < expressions.size() - 1; i++) {
            Expression e1 = expressions.get(i);
            for (int j = i + 1; j < expressions.size(); j++) {
                Expression e2 = expressions.get(j);

                if (e1.isEqual(e2)) {
                    expressions.remove(e2);
                }
            }
        }

    }

}
