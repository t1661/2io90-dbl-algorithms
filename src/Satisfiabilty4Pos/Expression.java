/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satisfiabilty4Pos;

import DataStructure.Label;

public class Expression {

    public boolean label_1_onRight;
    public boolean label_2_onRight;

    public Label label_1;
    public Label label_2;

    public Expression(Label L1, boolean L1onRight, Label L2, boolean L2onRight) {
        label_1 = L1;
        label_2 = L2;
        label_1_onRight = L1onRight;
        label_2_onRight = L2onRight;
    }

    public void print() {
        String l1 = "[" + label_1.xBase + ", " + label_1.yBase + "]";
        String l2 = "[" + label_2.xBase + ", " + label_2.yBase + "]";
        System.out.println("(" + (label_1_onRight ? "" : "-") + l1 + " V " + (label_2_onRight ? "" : "-") + l2 + ")");
    }

    public boolean isEqual(Expression e) {
        return (label_1.samePos(e.label_1)
                && label_2.samePos(e.label_2)
                && label_1_onRight == e.label_1_onRight
                && label_2_onRight == e.label_2_onRight)
                || (label_1.samePos(e.label_2)
                && label_2.samePos(e.label_1)
                && label_1_onRight == e.label_2_onRight
                && label_2_onRight == e.label_1_onRight);
    }
}
