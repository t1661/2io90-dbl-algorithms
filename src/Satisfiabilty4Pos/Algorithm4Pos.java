/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satisfiabilty4Pos;

import java.util.ArrayList;
import DataStructure.*;

public class Algorithm4Pos {

    LabelsContainer output;

    ArrayList<LabelExt4Pos> labelExtentions = new ArrayList();

    public Algorithm4Pos(DataStructure.LabelsContainer lc) {
        output = lc;

        setupStuff();
    }

    private void setupStuff() {
        int allLabelsPre = output.labelList.size();

        for (int i = 0; i < allLabelsPre; i++) {
            labelExtentions.add(new LabelExt4Pos(output.labelList.get(i)));
        }

    }

    public void execute() {
        for (int i = 10; i <= 100; i++) {
            System.out.println(i);
            output.setHeight(i);
            ArrayList<Expression> exp = getEpxressionsOptimized();
            if (exp != null) {
                continue;
            } else {
                output.setHeight(i - 1);
                break;
            }
        }
    }

    private ArrayList<Expression> getEpxressionsOptimized() {
        for (Label l : output.labelList) {
            l.Attributes.put(ExcludedMiddle.class, new ExcludedMiddle());
        }

        ArrayList<Expression> expressions = new ArrayList();
        CreateExpressions.create(expressions, output);

        System.out.println(" EXPRESSION LIST ");
        for (Expression e : expressions) {
            e.print();
        }

        boolean failed = false;
        while (true) {
            UPDATE update = updateExpression(expressions);

            System.out.println(" EXPRESSION LIST ");

            if (update == UPDATE.IMPOSSIBLE) {
                failed = true;
                break;
            } else if (update == UPDATE.END) {
                break;
            } else {
                //for (Expression e : expressions) {
                   // e.print();
                //}
                continue;
            }
        }

        if (failed) {
            return null;
        } else {
            return expressions;
        }
    }

    private enum UPDATE {

        IMPOSSIBLE, CONTINUE, END;
    }

    private UPDATE updateExpression(ArrayList<Expression> expressions) {

        for (int i = 0; i < expressions.size() - 1; i++) {
            Expression E1 = expressions.get(i);

            for (int j = i + 1; j < expressions.size(); j++) {
                Expression E2 = expressions.get(j);

                Expression newExp = null;

                if (E1.label_1.samePos(E2.label_1)
                        && E1.label_1_onRight != E2.label_1_onRight) {
                    newExp = new Expression(E1.label_2, E1.label_2_onRight, E2.label_2, E2.label_2_onRight);
                } else if (E1.label_1.samePos(E2.label_2)
                        && E1.label_1_onRight != E2.label_2_onRight) {
                    newExp = new Expression(E1.label_2, E1.label_2_onRight, E2.label_1, E2.label_1_onRight);
                } else if (E1.label_2.samePos(E2.label_1)
                        && E1.label_2_onRight != E2.label_1_onRight) {
                    newExp = new Expression(E1.label_1, E1.label_1_onRight, E2.label_2, E2.label_2_onRight);
                } else if (E1.label_2.samePos(E2.label_2)
                        && E1.label_2_onRight != E2.label_2_onRight) {
                    newExp = new Expression(E1.label_1, E1.label_1_onRight, E2.label_1, E2.label_1_onRight);
                }

                if (newExp != null) {                          
                    ExcludedMiddle middle = (ExcludedMiddle) newExp.label_1.Attributes.get(ExcludedMiddle.class);
                    if (newExp.label_1.samePos(newExp.label_2) && newExp.label_1_onRight == newExp.label_2_onRight) {
                        if (newExp.label_1_onRight) {
                            middle.bothPos = true;
                        } else {
                            middle.bothNeg = true;
                        }
                        if (middle.bothPos && middle.bothNeg) {
                            return UPDATE.IMPOSSIBLE;
                        }

                    }
                    
                    System.out.println("COMBINE : ");
                    E1.print();
                    E2.print();
                    System.out.println("into: ");
                    newExp.print();
                    System.out.println();
                    
                    expressions.remove(E1);
                    expressions.remove(E2);
                    expressions.add(newExp);
                    
                    return UPDATE.CONTINUE;
                }
            }
        }

        return UPDATE.END;
    }

    public class ExcludedMiddle {

        public boolean bothPos = false;
        public boolean bothNeg = false;
    }

    private void cleanup() {
        for (LabelExt4Pos l : labelExtentions) {
            l.close();
        }
    }
}
