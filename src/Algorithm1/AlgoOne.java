package Algorithm1;

import DataStructure.PlacementAvailability;
import DataStructure.*;
import group5.ReadInput;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;


/**
 *
 * @author Bart
 */
public class AlgoOne {

    public static boolean show = false;

    public static Label lastFail = null;
    
    Random random = new Random();

    //based on 4 pos
    LabelsContainer lc;

    public AlgoOne(LabelsContainer lc) {
        this.lc = lc;
    }

    // improovements:
    // keep positions from last try
    // start with low H and go up : or at least a better approximation to shift the height)
    // for example
    // first exponential search starting at 1
    // this gives an lowH (possible) and a highH (not possible)
    // make a backup of the lowH working version every step after we know it works
    // do binary search
    public void execute() {
        // determine start height

        // loop trough all blocks
        // can we delete positions that are disallowed directly?
        // quite slow right now
        //float lowH = 0.45f;
        //float highH = 32.0f;
        //float center = lowH + (highH - lowH) / 2.0f;
        float   curHeight = 1.0f * (lc.ratio > 1 ? 1.0f / lc.ratio : 1);
        boolean expFaze = true;
        float   maxSolve = 0;

        float   delta = 0.5f;
        int     face2IterateCount = 0;
        int     targetIterate = 1;      // just keep 1 to avoid stupid stuff
        int     maxIterate = 30;
        int     maxIterateSameH = 2;
        float   lowH = 0;
        float   highH = 0;

        boolean failed;

        int     masterBackup = 0;

        while (true) {

            lc.setHeight(curHeight);

            //if (show) 
            System.out.println(curHeight);

            if (!expFaze) {
                face2IterateCount += 1;
            }

            failed = false;

            failed = !setLabelPointOverlaps();

            if (!failed) {

                boolean allSolved = true;

                for (Label l : lc.labelList) {
                    boolean solved = solveIntersectionStrong(l, 0, null);
                    if (!solved) {
                        allSolved = false;
                        lastFail = l;
                        break;
                    }
                }

                if (allSolved) {
                    failed = false;
                }

                // loop trough all labels
                // while unsolved
                // try solving it by changed the placement of this thing
                // if not possible, use the list of the ones it intersects with and try to stop intersection with them     
                if (!failed) {
                    failed = lc.hasAnyIntersection();
                }
            }

            if (show) {
                show();
            }

            if (face2IterateCount == maxIterate) {
                if (!failed) {
                    return;
                } else {
                    lc.setHeight(lowH);
                    if (show) {
                        show();
                    }
                    return;
                }
            } else if (face2IterateCount >= targetIterate) {
                if (!failed) {
                    return;
                } else {
                    lowH *= 0.99;
                    if (lowH < maxSolve) {
                        lc.restoreBackup(masterBackup, true);
                        return;
                    }
                }
            }

            // decide next height
            if (expFaze) {
                if (failed) {
                    expFaze = false;
                    highH = curHeight;
                    lowH = curHeight / 2;
                    curHeight = lowH + (highH - lowH) / 2.0f;
                    targetIterate = (int) (Math.min(maxIterate, 1 + Math.log((highH - lowH) / delta) / Math.log(2)));
                } else {
                    // make backup of current labels
                    masterBackup = lc.createBackup();
                    maxSolve = curHeight;
                    curHeight *= 2;
                    if (curHeight < 2 && curHeight > 1) {
                        curHeight = (int) curHeight;
                    } // avoid infinite loop, if there is no overlap (example : 1 label) it would otherwise try infinite size.
                    else if (curHeight > 4294967296.0f) {
                        return;
                    }
                }
            } else {
                if (failed) {
                    highH = curHeight;
                } else {
                    lc.removeBackup(masterBackup);
                    masterBackup = lc.createBackup();
                    lowH = curHeight;
                    maxSolve = curHeight;
                }
                curHeight = lowH + (highH - lowH) / 2.0f;
            }

            // make attributes ready for next loop:
            for (Label l : lc.labelList) {
                l.PA.reset();
            }

        }

    }

    public boolean printHT = true;

    public void execute2() {

        long timeStamp = System.currentTimeMillis();

        int afterExpLimit = 100;

        //float currentH = 1.0f * (lc.ratio > 1 ? 1.0f / lc.ratio : 1);

        lc.setHeight(10);
        lc.randomizePositions();
        lc.snapDown();  
        float currentH = lc.getHeight();
        
        int bestSolution = 0;

        float lowH = 0;
        float highH = 0;

        // phase 1
        while (true) {

            if (printHT) {
                System.out.println(currentH + " time: " + (System.currentTimeMillis() - timeStamp));
            }

            boolean overlapError = !setLabelPointOverlaps();

            if (!overlapError) {
                solveStrongLoop();
            }

            // now determine the next step
            if (overlapError || lc.hasAnyIntersection()) {
                lowH = lc.getHeightFromMemory(bestSolution);
                highH = currentH;
                break;
            } else {
                if (currentH > 9999999) {
                    return;
                }
                bestSolution = lc.createBackup();
                currentH *= 2;
                // if (currentH > 1) currentH = (int) currentH;
            }

            // prepare for new height by resetting the available positions
            for (Label l : lc.labelList) {
                l.PA.reset();
            }
            lc.setHeight(currentH);
        }
        
        float snapDelta = 0.5f;

        // phase 2
        int loops;
        boolean failed = false;
        for (loops = 0; loops < afterExpLimit; loops++) {

            currentH = lowH + (highH - lowH) / 2.0f;
            lc.setHeight(currentH);

            if (printHT) {
                System.out.println(" time: " + (System.currentTimeMillis() - timeStamp) + "  C:" +currentH+ "  L:"+ lowH + ", H:" + highH);
            }

            boolean overlapError = !setLabelPointOverlaps();

            if (overlapError) {
                failed = true;
            } else {
                solveStrongLoop();
                failed = lc.hasAnyIntersection();
            }
            
            if (failed) {
                highH = currentH;
            } else {
                lowH = currentH;
                bestSolution = lc.createBackup();
            }

            if ((highH - lowH) <= snapDelta) {
                if (failed) {
                    // now snap down from the current height
                    // but with the positions of the last solve
                    lc.restoreBackupWithoutHeight(bestSolution, false);
                    lc.snapDown();
                } else {
                    // now we snap down from the high version
                    // with the positions of the last solve
                    lc.restoreBackupWithoutHeight(bestSolution, false);
                    lc.setHeight(highH);
                    lc.snapDown();
                }
                return; // END OF THE ALGORITHM
            }

            // prepare for new height by resetting the available positions
            for (Label l : lc.labelList) {
                l.PA.reset();
            }
        }

        if (loops == afterExpLimit) {
            lc.restoreBackup(bestSolution, false);
        }
    }

    private boolean setLabelPointOverlaps() {
        for (Label l : lc.labelList) {
            //l.setPosition(LabelPosition.TopRight);
            ArrayList<Label> rList = l.getPointsInRange();
            PlacementAvailability lAv = l.PA;

            for (Label l2 : rList) {
                boolean[] posPossible = l.positionsPossible(l2);
                lAv.pushImpossible(posPossible);
            }

            if (lAv.availables.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private void solveStrongLoop() {
        for (Label l : lc.labelList) {
            boolean solved = solveIntersectionStrong(l, 0, null);
            if (!solved) {
                lastFail = l;
                break;
            }
        }
    }

    private void solveFastLoop() {
        for (Label l : lc.labelList) {
            boolean solved = solveIntersectionFast(l, 0);
            if (!solved) {
                lastFail = l;
            }
        }
    }

    public void setForcePosition() {
        for (Label l : lc.labelList) {
            l.Attributes.put(LabelsContainer.Force.class, lc.new Force());
        }

        for (int i = 0; i < lc.labelList.size() - 1; i++) {
            Label l1 = lc.labelList.get(i);
            LabelsContainer.Force l1Force = (LabelsContainer.Force) l1.Attributes.get(LabelsContainer.Force.class);
            for (int j = i + 1; j < lc.labelList.size(); j++) {
                Label l2 = lc.labelList.get(j);
                LabelsContainer.Force f = l1.forceFrom(l2);

                l1Force.addFromV2(f);
                ((LabelsContainer.Force) l2.Attributes.get((LabelsContainer.Force.class))).addInvFromV2(f);
            }
        }

        for (Label l : lc.labelList) {
            LabelsContainer.Force f = (LabelsContainer.Force) l.Attributes.get(LabelsContainer.Force.class);
            if (f.x < 0) {
                if (f.y < 0) {
                    l.setPosition(LabelPosition.TopRight);
                } else {
                    l.setPosition(LabelPosition.BottomRight);
                }
            } else {
                if (f.y < 0) {
                    l.setPosition(LabelPosition.TopLeft);
                } else {
                    l.setPosition(LabelPosition.BottomLeft);
                }
            }
        }
    }

    private void show() {
        lc.setAllIntersections();

        Graphics.Display.setContent(lc);
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
    }

    private boolean solveIntersectionStrong(Label l1, int depth, Label parent) {
        // we are trying all possibilities recursively to solve intersections
        // we first try the 4 positions for this label
        // if one of them doesn't intersect we return that branch
        // if all of them intersect, go trough them and try to solve the ones we intersect with
        // maby somehow avoid all the labels that are " parents "
        
        ArrayList<Label> iListOrigin = l1.intersectList();
        if (iListOrigin.size() == 0) {
            return true;
        }

        // try the other positions for this thing:      
        PlacementAvailability PA = l1.PA;

        LabelPosition originPos = l1.getPosition();
        if (l1.hasIntersection() == null) {
            return true;
        }

        for (LabelPosition lp : PA.availables) {
            if (lp != originPos) {
                l1.setPosition(lp);
                if ((parent == null || !l1.doesIntersectWith(parent)) && l1.hasIntersection() == null) {
                    return true;
                }
            }
        }

        if (depth >= 4) {
            l1.setPosition(originPos);
            return false;
        }
        // it didnt directly work, now do a branch for esach position:

        int mem = lc.createBackup();

        ArrayList<Label> iList;
        for (LabelPosition lp : PA.availables) {
            l1.setPosition(lp);
            
            if (lp != originPos)
                iList = l1.intersectList();
            else iList = iListOrigin;
            
            boolean interSectsParent = iList.remove(parent);
            
            boolean positionSolved = true;
            if (!interSectsParent) for (Label l2 : iList) {
                 if (!solveIntersectionStrong(l2, depth + 1, l1)) {
                    positionSolved = false;
                    break;
                }
            }
            else {
                positionSolved = false;
            }
            if (positionSolved) {
                lc.removeBackup(mem);
                return true;
            } else {
                lc.restoreBackup(mem, false);
            }
        }

        return false;
    }

    private boolean solveIntersectionFast(Label l1, int depth) {

        PlacementAvailability PA =  l1.PA;

        ArrayList<ArrayList<Label>> intersectMemory = new ArrayList();
        ArrayList<LabelPosition> intersectMemoryPositions = new ArrayList();

        // go over all positions to see if there is a free one
        // start with the one it is allready in, and avoid it later in the loop
        LabelPosition originalP = l1.getPosition();
        ArrayList<Label> iList = l1.intersectList();
        if (iList.isEmpty()) {
            return true;
        } else {
            intersectMemoryPositions.add(originalP);
            intersectMemory.add(iList);
        }
        for (LabelPosition lp : PA.availables) {
            if (lp != originalP) { // ovoiding original position
                l1.setPosition(lp);
                if (show) {
                    show();
                }
                iList = l1.intersectList();
                if (iList.isEmpty()) {
                    return true;
                } else {
                    intersectMemoryPositions.add(lp);
                    intersectMemory.add(iList);
                }
            }
        }

        // no position gave good results
        // sort by minimal intersection problems and try to solve them
        if (depth >= 3) {
            return false;
        }

        while (!intersectMemory.isEmpty()) {

            int minID = 0;

            for (int i = 1; i < intersectMemory.size(); i++) {
                if (intersectMemory.get(i).size() < intersectMemory.get(minID).size()) {
                    minID = i;
                }
            }

            LabelPosition thisPos = intersectMemoryPositions.remove(minID);

            l1.setPosition(thisPos);
            if (show) {
                show();
            }

            boolean allSolved = true;
            for (Label l : intersectMemory.get(minID)) {
                if (!solveIntersectionFast(l, depth + 1)) {
                    allSolved = false;
                    break;
                }
            }

            if (allSolved) {
                return true;
            }

            intersectMemory.remove(minID);
        }

        return false;
    }

    private boolean solveIntersectionRandom(Label l1, int depth)
    {
        
        return false;
    }
    
    public static void runTimeTest() {
        DataStructure.LabelsContainer newData = null;
        
        try {
            newData = ReadInput.loadInputFromFile("4pos10000"); //inputReader.loadRandom(100000, Placement.TwoPos, 1, 3); 
        } catch (Exception e) {
            return;
        }
        newData.setHeight(1);

        int pCountMax = 100000;

        for (int pC = 100; pC <= pCountMax; pC += 10) {
            newData = ReadInput.loadRandom(pC, Placement.FourPos, 1.0f, 5.0f);
            AlgoOne algoOne = new AlgoOne(newData);

            long millisStart = System.currentTimeMillis();
            algoOne.execute();
            long totalMillis = System.currentTimeMillis() - millisStart;

            System.out.println(pC + " \t" + totalMillis);
        }

    }

    public static void testAlgo() {
        Scanner sc = new Scanner(System.in);
        DataStructure.LabelsContainer newData = null;
        int i;
        int count = 1000;
        //AlgoOne.show = true;
        long totalMillis = 0;

        for (i = 0; i < count; i++) {
            newData = ReadInput.loadRandom(100, Placement.FourPos, 1.13f, 5.0f);

            if (show) {
                newData.setAllIntersections();
                Graphics.Display.setContent(newData);
                sc.nextLine();
            }

            AlgoOne algoOne = new AlgoOne(newData);

            long millisStart = System.currentTimeMillis();
            algoOne.execute();
            totalMillis += System.currentTimeMillis() - millisStart;

            if (show) {
                System.out.println(" time : " + totalMillis + " Height : " + newData.getHeight());
                newData.setAllIntersections();
                Graphics.Display.setContent(newData);
                sc.nextLine();
            }

            if (newData.hasAnyIntersection()) {
                break;
            }

            if (i % (count / 100) == 0) {
                System.out.println(100 * i / count + "%");
            }

        }
        System.out.println();
        if (i != count) {
            System.out.println(" ERROR | height: ");

            AlgoOne.show = true;

            AlgoOne algoOne = new AlgoOne(newData);
            algoOne.execute();
        } else {
            System.out.println("SUCCES time:" + (totalMillis / count) + " per solve " + newData.getHeight());
        }

        newData.setAllIntersections();
        Graphics.Display.setContent(newData);

    }

}
