package Graphics;

import java.util.ArrayList;
import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;

public class ViewBlock implements Runnable {
        
    int xId;
    int yId;
    public volatile boolean DRAW_READY = false;
    public Vector2i worldIdLoc;
    ArrayList<GraphicsElement> graphicsElements;
    private long timeouStamp;
    

    public ViewBlock(int X, int Y) {
        xId = X;
        yId = Y;

        constructorExtention();
    }

    public ViewBlock(Vector2i v) {
        xId = v.x;
        yId = v.y;

        constructorExtention();
    }

    private void constructorExtention() {
        worldIdLoc = new Vector2i(xId * ViewBlockManager.blockIdSize, yId * ViewBlockManager.blockIdSize);
        resetTimeout();
        graphicsElements = new ArrayList();
    }

    public boolean inView(DrawInfo frInf) {
        boolean H_overlap = worldIdLoc.x <= frInf.RT_x && worldIdLoc.x + ViewBlockManager.blockIdSize >= frInf.LB_x;
        boolean V_overlap = worldIdLoc.y <= frInf.RT_y && worldIdLoc.y + ViewBlockManager.blockIdSize >= frInf.LB_y;
        return H_overlap && V_overlap;
    }

    public void draw(RenderWindow RW, DrawInfo info) {
        Transform tr = Transform.translate(
                Transform.IDENTITY,
                info.screenPosCoord(Vector2f.ZERO)
        );

        RenderStates states = new RenderStates(tr);
        for (int i = 0; i < graphicsElements.size(); i++) {
            graphicsElements.get(i).draw(RW, states);
        }
    }

    @Override
    public void run() {
        loadData();
    }

    //if changes are needed, you just create a new viewblock
    //and keep drawing this one until the new one is ready
    private void loadData() {
        if (Display.data != null)
        {
            // load stuff
            graphicsElements.add(
                new LabelGroup(Display.data, worldIdLoc, 
                new Vector2i(worldIdLoc.x + ViewBlockManager.blockIdSize, worldIdLoc.y + ViewBlockManager.blockIdSize)));
            
        }
        DRAW_READY = true;
        resetTimeout();
    }

    public void resetTimeout() {
        timeouStamp = System.currentTimeMillis();
    }

    public boolean isTimedOut() {
        if(!DRAW_READY) return false;
        return System.currentTimeMillis() - timeouStamp >= 4000;
    }

}
