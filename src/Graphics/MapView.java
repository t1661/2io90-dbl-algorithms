package Graphics;

import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;

public class MapView extends CView {

    MapOrientation orient;
    DrawInfo drawInfo;
    ViewBlockManager viewBlockManager;

    public MapView(RenderWindow rw, Vector2i size, Vector2i topLeft) {
        super(rw, size, topLeft);
        constructorExtention();
    }
    
    public void autoFit(float x, float y)
    {
        orient.setZoom((float)Math.min(x, y) / (0.666f*720.0f));
        orient.setCenter(x/2, y/2);
    }

    public MapView(RenderWindow rw, Vector2i size) {
        super(rw, size);
        constructorExtention();
    }

    public MapView(RenderWindow rw) {
        super(rw);
        constructorExtention();
    }

    private void constructorExtention() {
        background.setFillColor(new Color(80, 80, 80));

        //create orientation
        orient = new MapOrientation(this);
        zoomChanged();

        //initialize static values:
        drawInfo = new DrawInfo(orient, VirtualPixelSize(), 10);
        viewBlockManager = new ViewBlockManager(drawInfo);
    }

    @Override
    public void draw(RenderWindow RW) {
        RW.setView(sfmlView);

        drawInfo.updateInfo(orient, VirtualPixelSize(), 10);

        //RW.draw(background);

        viewBlockManager.draw(RW, drawInfo);
    }

    public void update(boolean contentChanged) {
        viewBlockManager.update(drawInfo, contentChanged);
    }

    @Override
    public void drag(Vector2i direction) {
        orient.drag(direction);
    }

    @Override
    public void click(Vector2i localClickPos) {
        //System.out.println(localClickPos);
    }

    @Override
    public void scroll(MouseWheelEvent e) {
        orient.zoom(e.delta);
    }

    public void zoomChanged() {
        //elements.AdaptToZoom(orient.scale);
        setZoom(orient.scale.x);
    }
}

class MapOrientation {

    MapView mv;
    public Vector2f center;
    public Vector2f scale;

    final float initScale = 0.05f;

    public void zoom(float delta) {
        float scaling = 1.0f - delta / 10.1f;
        scale = clamp(new Vector2f(scale.x * scaling, scale.y * scaling), 0.01f, 20.0f);
        mv.zoomChanged();
    }
    
    public void setZoom(float z)
    {
        scale = clamp(new Vector2f(z, z), 0.01f, 20.0f);
        mv.zoomChanged();
    }
    public void setCenter(float x, float y)
    {
        center = new Vector2f(x,y);
    }

    public void drag(Vector2i dir) {       
        center = new Vector2f(-1.0f * dir.x * scale.x + center.x, dir.y * scale.y + center.y);
        //System.out.println(center);
    }

    private static Vector2f clamp(Vector2f v, float low, float high) {
        return new Vector2f(
                v.x < low ? low : (v.x > high ? high : v.x),
                v.y < low ? low : (v.y > high ? high : v.y)
        );
    }

    public MapOrientation(MapView MV) {
        mv = MV;
        center = new Vector2f(0, 0);
        scale = new Vector2f(initScale, initScale);
    }

    public MapOrientation Copy() {
        MapOrientation copy = new MapOrientation(mv);
        copy.center = center;
        copy.scale = scale;
        return copy;
    }
}
