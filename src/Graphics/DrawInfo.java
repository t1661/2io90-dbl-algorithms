/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

import org.jsfml.system.Vector2f;


public class DrawInfo {

    private Vector2f virtualPixelContent;
    public MapOrientation orient;
    public Vector2f drawAreaSize_world;
    public float LB_x;
    public float LB_y;
    public float RT_x;
    public float RT_y;
    private float sideOffset;

    public DrawInfo(MapOrientation orientation, Vector2f VPC, float worldDrawAreaOffset) {
        updateInfo(orientation, VPC, worldDrawAreaOffset);
    }

    public void updateInfo(MapOrientation orientation, Vector2f VPC, float worldDrawAreaOffset) {
        orient = orientation;
        sideOffset = worldDrawAreaOffset;

        virtualPixelContent = VPC;
        
        Vector2f bottomLeftDrawArea = new Vector2f(
            orient.center.x - virtualPixelContent.x * 0.5f - sideOffset,
            orient.center.y - virtualPixelContent.y * 0.5f - sideOffset
        );
        
        drawAreaSize_world = new Vector2f(
            virtualPixelContent.x + sideOffset * 2,
            virtualPixelContent.y + sideOffset * 2
        );

        LB_x = bottomLeftDrawArea.x; 
        LB_y = bottomLeftDrawArea.y;

        RT_x = LB_x + drawAreaSize_world.x;
        RT_y = LB_y + drawAreaSize_world.y;
    }

    public Vector2f screenPosCoord(Vector2f worldPos) {
        float x = worldPos.x - orient.center.x + virtualPixelContent.x * 0.5f;
        float y = -(worldPos.y - orient.center.y + virtualPixelContent.y * 0.5f) + virtualPixelContent.y;
        
        return new Vector2f(x, y);
    }
}
