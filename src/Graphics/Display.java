package Graphics;

import java.util.ArrayList;
import java.util.Random;
import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import group5.ReadInput;

import DataStructure.*;
import java.io.FileNotFoundException;

public class Display implements Runnable {

    Color clearColor = new Color(80, 80, 80);
    RenderWindow window;
    WindowContent windowContent;

    String windowName = "SFML";

    public static volatile LabelsContainer data = null;
    public static volatile LabelsContainer newData = null;
    public static volatile boolean dataRenewed = false;
    public static volatile boolean autofit = false;
    public final static Object lock = new Object();

    private int FPSlimit = 120;
    private boolean vSync = false;
    private int antialiasing = 2;

    @Override
    public void run() {
        ContextSettings renderSettings = new ContextSettings(antialiasing);
        int style = WindowStyle.CLOSE | WindowStyle.TITLEBAR;

        window = new RenderWindow(new VideoMode(1280, 720), windowName, style, renderSettings);
        window.clear(new Color(0, 0, 0));

        MainLoop();
    }

    private void MainLoop() {
        windowContent = new WindowContent(window);

        window.setVerticalSyncEnabled(vSync);
        window.setFramerateLimit(FPSlimit);
        window.setPosition(new Vector2i(500, 100));
        // create basic window content

        try {
            window.setActive(true);
        } catch (Exception e) {

        }

        while (window.isOpen()) {
            
            for (Event event : window.pollEvents()) {
                if (event.type == Event.Type.CLOSED) {
                    window.close();
                    System.exit(0);
                }
                if (event.type == Event.Type.MOUSE_BUTTON_PRESSED) {
                    windowContent.MouseClick(event.asMouseButtonEvent());
                }
                if (event.type == Event.Type.MOUSE_WHEEL_MOVED) {
                    windowContent.MouseWheel(event.asMouseWheelEvent());
                }
                if (event.type == Event.Type.RESIZED) {
                    windowContent.defineViewSizes();
                }
            }

            window.clear(clearColor);

            windowContent.draw();

            boolean contentChanged;

            synchronized (lock) {
                contentChanged = newData != data || dataRenewed;
                data = newData;
                dataRenewed = false;
            }
            
            if (autofit)
            {
                autofit = false;
                if (data != null) windowContent.autoFit(data.getMaxX(), data.getMaxY());            
            }

            windowContent.update(contentChanged);
            window.display();

        }
    }

    public static void setContent(LabelsContainer v) {
        synchronized (lock) {
            newData = v;
            dataRenewed = true;
        }
    }

    public static void autoFit()
    {
        autofit = true;
    }
}

class WindowContent {

    RenderWindow window;
    MouseManager mouseManager;
    View fullView;
    MapView mapView;

    public WindowContent(RenderWindow RW) {
        window = RW;
        mouseManager = new MouseManager(window);

        mapView = new MapView(window);
        defineViewSizes();
    }

    public void NewInput() {

    }

    public void defineViewSizes() {
        fullView = new View(new FloatRect(0, 0, window.getSize().x, window.getSize().y));
        setMapView();
    }

    public void MouseClick(MouseButtonEvent e) {
        if (!mouseManager.buzy) {
            if (mapView.clickable && mapView.contains(e.position.x, e.position.y)) {
                mouseManager.start(mapView, e);
            }
        }
    }

    public void MouseWheel(MouseWheelEvent e) {
        if (!mouseManager.buzy) {
            if (mapView.scrollable && mapView.contains(e.position.x, e.position.y)) {
                mapView.scroll(e);
            }
        }
    }

    private void setMapView() {
        //actual border offset in pixels:
        int leftOffset = 0;
        int topOffset = 0;
        int rightOffset = 0;
        int bottomOffset = 0;

        mapView.setAll(
                new Vector2i(window.getSize().x - (leftOffset + rightOffset),
                        window.getSize().y - (topOffset + bottomOffset)),
                new Vector2i(leftOffset, topOffset));
    }

    public void draw() {
        mapView.draw(window);
    }

    public void update(boolean contentChanged) {
        mouseManager.update();
        mapView.update(contentChanged);
    }
    
    public void autoFit(float x, float y)
    {
        mapView.autoFit(x,y);
    }

}

class MouseManager {

    final float distToDrag = 3;
    Vector2i startPosInWindow;
    Vector2i prevPosRel;
    CView view;
    RenderWindow window;

    //reset values:
    public boolean buzy;
    boolean maxDistReached = false;

    public MouseManager(RenderWindow RW) {
        buzy = false;
        window = RW;
    }

    public void start(CView v, MouseButtonEvent e) {
        startPosInWindow = Mouse.getPosition();
        prevPosRel = new Vector2i(0, 0);
        buzy = true;
        view = v;
    }

    public void update() {
        if (buzy) {
            if (Mouse.isButtonPressed(Mouse.Button.LEFT)) {
                Vector2i newPosRel = new Vector2i(
                        Mouse.getPosition().x - startPosInWindow.x,
                        Mouse.getPosition().y - startPosInWindow.y
                );
                if (!maxDistReached) {
                    maxDistReached = length(newPosRel) >= distToDrag;
                }
                if (maxDistReached && (newPosRel.x != prevPosRel.x || newPosRel.y != prevPosRel.y)) {
                    Vector2i dragDir = new Vector2i(newPosRel.x - prevPosRel.x, newPosRel.y - prevPosRel.y);
                    prevPosRel = newPosRel;
                    view.drag(dragDir);
                }
            } else {
                release();
            }
        }
    }

    public static float length(Vector2i v) {
        return (float) Math.sqrt(v.x * v.x + v.y * v.y);
    }

    private void release() {
        if (!maxDistReached) {
            view.click(
                    new Vector2i(
                            (startPosInWindow.x) - view.viewTopLeft.x,
                            (startPosInWindow.y) - view.viewTopLeft.y
                    ));
        }
        buzy = false;
        maxDistReached = false;
    }
}
