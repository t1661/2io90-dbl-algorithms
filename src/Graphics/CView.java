/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.FloatRect;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.View;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;

/**
 *
 * @author Bart
 */

abstract class CView {

    //a view has the "sfml view" class, (can contain other views aswell??)
    protected RenderWindow RW;
    protected View sfmlView = new View();
    public Vector2i viewSize;
    public Vector2i viewTopLeft;

    private float viewZoom = 1;

    public boolean clickable = true;
    public boolean scrollable = true;

    protected RectangleShape background = new RectangleShape();
    private final Color default_background_color = new Color(0, 0, 0);

    public CView(RenderWindow rw) {
        RW = rw;
        viewSize = new Vector2i((int) RW.getSize().x, (int) RW.getSize().y);
        viewTopLeft = new Vector2i(0,0);
        background.setFillColor(default_background_color);
        reBuild();

    }

    public CView(RenderWindow rw, Vector2i Size) {
        RW = rw;
        viewSize = Size;
        viewTopLeft = new Vector2i(0,0);
        background.setFillColor(default_background_color);
        reBuild();
    }

    public CView(RenderWindow rw, Vector2i Size, Vector2i TopLeft) {
        RW = rw;
        viewSize = Size;
        viewTopLeft = TopLeft;
        background.setFillColor(default_background_color);
        reBuild();
    }

    protected Vector2f VirtualPixelSize() {
        return sfmlView.getSize();
    }

    protected void setZoom(float z) {
        viewZoom = z;
        reBuild();
    }

    public void setSize(Vector2i newSize) {
        viewSize = newSize;
        reBuild();
    }

    public void setPosition(Vector2i TopLeft) {
        viewTopLeft = TopLeft;
        reBuild();
    }

    public void setAll(Vector2i newSize, Vector2i newTopLeft) {
        viewSize = newSize;
        viewTopLeft = newTopLeft;
        reBuild();
    }

    private void reBuild() {
        sfmlView = new View(new FloatRect(0, 0,
                viewSize.x * viewZoom,
                viewSize.y * viewZoom
        ));

        //calculation viewport with relative values:
        Vector2f topLeftOffsetRel = new Vector2f(viewTopLeft.x / RW.getSize().x, viewTopLeft.y / RW.getSize().y);          
        Vector2f sizeRel = new Vector2f(viewSize.x / RW.getSize().x, viewSize.y / RW.getSize().y);   

        sfmlView.setViewport(new FloatRect(
                topLeftOffsetRel.x,
                topLeftOffsetRel.y,
                sizeRel.x,
                sizeRel.y
        ));

        background.setSize(new Vector2f(viewSize));
    }

    public boolean contains(int X, int Y) {
        return X >= viewTopLeft.x
                && X <= viewTopLeft.x + viewSize.x
                && Y >= viewTopLeft.y
                && Y <= viewTopLeft.y + viewSize.y;
    }

    public void draw(RenderWindow RW)
    {
        
    }

    public void drag(Vector2i direction) {
    }

    public void click(Vector2i localClickPos) {
    }

    public void scroll(MouseWheelEvent e) {
        
    }

}
