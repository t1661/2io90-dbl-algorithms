package Graphics;

import org.jsfml.graphics.*;
import org.jsfml.system.*;
import DataStructure.*;

public class LabelGroup extends GraphicsElement {

    static int dotsCount1D;
    static Color color;
    final float worldRad = 0.1f;
    
    final Color noIntersect = new Color(20,160,20,190);
    final Color intersect = new Color(250,0,0,190);

    private VertexArray VAO = null;

    public LabelGroup(LabelsContainer data, Vector2i LB, Vector2i RT) {
        build(data,LB,RT);
    }

    private void build(LabelsContainer data, Vector2i LB, Vector2i RT) {
        VAO = new VertexArray(PrimitiveType.QUADS);
        
        for (int i = 0; i < data.labelList.size(); i++) {
            
            Label l = data.labelList.get(i);
            
            if (l.xBase >= LB.x && l.yBase >= LB.y
             && l.xBase < RT.x && l.yBase < RT.y)
            {                          
                Color c = l.hasIntersectForvisual ? intersect : noIntersect;

                Vertex lt = new Vertex(new Vector2f(l.left(), -l.top()), c);
                Vertex rt = new Vertex(new Vector2f(l.right(), -l.top()), c);
                Vertex rb = new Vertex(new Vector2f(l.right(), -l.bottom()), c);
                Vertex lb = new Vertex(new Vector2f(l.left(), -l.bottom()), c);      

                VAO.add(lt);
                VAO.add(rt);
                VAO.add(rb);
                VAO.add(lb);
            }
            
        }
        
        for (int i = 0; i < data.labelList.size(); i++) {
            
            Label v = data.labelList.get(i);
            
            if (v.xBase >= LB.x && v.xBase < RT.x && v.yBase >= LB.y && v.yBase < RT.y ) {
                float offset = 0.4f;
                Color c = Color.BLACK;

                Vertex top = new Vertex(new Vector2f(v.xBase, -(v.yBase + offset)), c);
                Vertex bottom = new Vertex(new Vector2f(v.xBase, -(v.yBase - offset)), c);
                Vertex left = new Vertex(new Vector2f(v.xBase - offset, -v.yBase), c);
                Vertex right = new Vertex(new Vector2f(v.xBase + offset, -v.yBase), c);

                VAO.add(top);
                VAO.add(right);
                VAO.add(bottom);
                VAO.add(left);
            }
        }
    }
    

    @Override
    public void draw(RenderWindow RW, RenderStates states) {
        //states.Texture = Globals.data.textures.defaultDot;
        RW.draw(VAO, states);
    }
}
