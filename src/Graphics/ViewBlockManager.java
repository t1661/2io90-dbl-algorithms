package Graphics;

import java.util.HashMap;
import java.util.Iterator;

import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;

class Key {

    int x;
    int y;

    public Key(int X, int Y) {
        x = X;
        y = Y;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Key) {
            Key otherKey = (Key) other;
            if (otherKey.x == x && otherKey.y == y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return x * y;
    }
}

public class ViewBlockManager {

    public static int blockIdSize = 800;
    
    float preloadRadRel = 0.01f;
    HashMap<Key, ViewBlock> loadedBlocks;


    public ViewBlockManager(DrawInfo info) {       
        update(info, true);
    }

    public void draw(RenderWindow RW, DrawInfo frInfo) {
        Iterator it = loadedBlocks.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pairs = (HashMap.Entry) it.next();
            ViewBlock vb = (ViewBlock) pairs.getValue();
            if (vb.DRAW_READY && vb.inView(frInfo)) {
                vb.draw(RW, frInfo);
            }
        }
    }

    private Vector2i VFloor(Vector2f v) {
        return new Vector2i((int) Math.floor(v.x), (int) Math.floor(v.y));
    }

    private Vector2i VCeiling(Vector2f v) {
        return new Vector2i((int) Math.ceil(v.x), (int) Math.ceil(v.y));
    }

    public void update(DrawInfo info, boolean contentChanged) {
        
        if (contentChanged) {
            loadedBlocks = new HashMap<Key, ViewBlock>();
        }
        
        Iterator it = loadedBlocks.entrySet().iterator(); 

        while (it.hasNext()) {
            HashMap.Entry pairs = (HashMap.Entry) it.next();
            ViewBlock vb = (ViewBlock) pairs.getValue();
            if (vb.isTimedOut()) {
                //it.remove();
            }
        }        
   
        int preLoadSize = (int) (preloadRadRel * (info.RT_x - info.LB_x));
        if (preLoadSize < 1) {
            preLoadSize = 1;
        }

        Vector2i LB_BLOCK_ID = VFloor(new Vector2f(
                (info.LB_x - preLoadSize) / (float) blockIdSize,
                (info.LB_y - preLoadSize) / (float) blockIdSize
        ));
        Vector2i RT_BLOCK_ID = VCeiling(new Vector2f(
                (info.RT_x + preLoadSize) / (float) blockIdSize,
                (info.RT_y + preLoadSize) / (float) blockIdSize
        ));

       
        for (int y = LB_BLOCK_ID.y; y <= RT_BLOCK_ID.y; y++) {
            for (int x = LB_BLOCK_ID.x; x <= RT_BLOCK_ID.x; x++) {
                if (!loadedBlocks.containsKey(new Key(x, y))) {
                    ViewBlock newBlock = new ViewBlock(new Vector2i(x, y));
                    loadedBlocks.put(new Key(x, y), newBlock);
                    newBlock.run();
                } else {
                    loadedBlocks.get(new Key(x, y)).resetTimeout();
                }
            }
        }

    }

}
