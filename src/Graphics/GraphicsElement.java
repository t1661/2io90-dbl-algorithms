
package Graphics;

import org.jsfml.graphics.*;

public abstract class GraphicsElement {
    public abstract void draw(RenderWindow RW, RenderStates states);
}
