/*
 * To change this license header, tryChoose License Headers in Project Properties.
 * To change this template file, tryChoose Tools | Templates
 * and open the template in the editor.
 */
package SatisFiability2Pos;

import DataStructure.LabelPosition;
import DataStructure.LabelsContainer;
import java.util.ArrayList;
import java.util.Collections;

public class SatGraphAlgo {

    ArrayList<Var> variables = new ArrayList();

    float height;
    float ratio;

    static boolean print = false;

    private void importVariables(LabelsContainer input) {
        for (DataStructure.Label l : input.labelList) {
            variables.add(new Var(l));
        }
        Collections.sort(variables);
    }

    public LabelsContainer execute(LabelsContainer input) {
        this.ratio = input.ratio;
        this.height = 9.01f;
        importVariables(input);
        for (DataStructure.Label l : input.labelList) {
            l.setPosition(LabelPosition.TopLeft);
        }

        ArrayList<OR> result = executeHeight();

        if (result == null) {
            System.out.println(" IMPOSSIBLE ");
        } else {
            System.out.println(" POSSIBLE ");
        }

        if (result != null) {

            for (OR or : result) {
                //or.print();
            }

            setPossibility(result);
        }

        input.setHeight(height);
        return null;
    }

    private void setPossibility(ArrayList<OR> relations) {

        ArrayList<Var> varsTemp = (ArrayList<Var>) variables.clone();

        for (int i = 0; i < varsTemp.size(); i++) {
            Var v = varsTemp.get(i);
            if (v.left.isForced) {
                v.state = VAR_STATE.Negated;
                varsTemp.remove(v);
            } else if (v.right.isForced) {
                v.state = VAR_STATE.Real;
                varsTemp.remove(v);
            }
        }

        for (Var v : varsTemp) {
            v.pushRelations(relations);
        }

        for (int i = 0; i < varsTemp.size();) {
            Var v = varsTemp.get(i);
            boolean canChoose = v.tryChoose();
            if (canChoose) {
                i += 1;
            } else {
                i -= 1;
            }
        }

        // for all variables: (not just the temporary list)
        for (Var v : variables) {
            v.setToOutput();
        }
    }

    private ArrayList<OR> executeHeight() {
        for (Var v : variables) {
            v.updateBounds();
        }
        ArrayList<OR> ORs = createBaseExpressions();
        if (ORs == null) {
            return null;
        }
        UPDATE update;
        while ((update = expressionUpdate(ORs)) == UPDATE.CONTINUE);
        if (update == UPDATE.IMPOSSIBLE) {
            return null;
        } else {
            return ORs;
        }
    }

    private ArrayList<OR> createBaseExpressions() {
        ArrayList<OR> ORs = new ArrayList();

        // the variables are allready sorted on x
        for (int i = 0; i < variables.size() - 1; i++) {
            Var a = variables.get(i);
            for (int j = i + 1; j < variables.size(); j++) {
                Var b = variables.get(j);
                // now there are 4 comparisons to be made
                if (!addExpression(a.left, b.left, ORs)) {
                    return null;
                }
                if (!addExpression(a.right, b.left, ORs)) {
                    return null;
                }
                if (!addExpression(a.left, b.right, ORs)) {
                    return null;
                }
                if (!addExpression(a.right, b.right, ORs)) {
                    return null;
                }
            }
        }

        return ORs;
    }

    // returns false if the (a V a ) AND (-a V -a) exception is made
    private boolean addExpression(POS a, POS b, ArrayList<OR> ORs) {

        if (a.doesIntersect(b)) {

            if (print) {
                System.out.println(" Adding rule for ");
                System.out.println(a.toString() + " | " + b.toString());
            }

            OR newOR = null;
            OR newORoptional = null;

            boolean sameSide = a.negated == b.negated;

            if (a.containsInclusive(b)) {
                if (sameSide) {
                    newOR = new OR(a.inverse, a.inverse);
                    newORoptional = new OR(b, b);
                    a.inverse.isForced = true;
                    b.isForced = true;
                    if (a.isForced) {
                        return false;
                    }
                    if (b.inverse.isForced) {
                        return false;
                    }
                } else {
                    newOR = new OR(a.inverse, a.inverse);
                    newORoptional = new OR(b.inverse, b.inverse);
                    a.inverse.isForced = true;
                    b.inverse.isForced = true;
                    if (a.isForced) {
                        return false;
                    }
                    if (b.isForced) {
                        return false;
                    }
                }
            } else if (b.containsInclusive(a)) {
                if (sameSide) {
                    newOR = new OR(b.inverse, b.inverse);
                    newORoptional = new OR(a, a);
                    b.inverse.isForced = true;
                    a.isForced = true;
                    if (b.isForced) {
                        return false;
                    }
                    if (a.inverse.isForced) {
                        return false;
                    }
                } else {
                    newOR = new OR(a.inverse, a.inverse);
                    newORoptional = new OR(b.inverse, b.inverse);
                    a.inverse.isForced = true;
                    b.inverse.isForced = true;
                    if (a.isForced) {
                        return false;
                    }
                    if (b.isForced) {
                        return false;
                    }
                }
            } else if (!sameSide) {
                newOR = new OR(a.inverse, b.inverse);
            } else if (a.v.x == b.v.x) {
                newOR = new OR(a, b);
                newORoptional = new OR(a.inverse, b.inverse);
            } else {
                // currently neigher one intersects the other
                // the they will in the reverse position
                // that one deals with this
            }

            if (newOR != null) {
                if (print) {
                    newOR.print();
                }
                if (!ORs.contains(newOR)) {
                    ORs.add(newOR);
                }
            }
            if (newORoptional != null) {
                if (print) {
                    newORoptional.print();
                }
                if (!ORs.contains(newORoptional)) {
                    ORs.add(newORoptional);
                }
            }
        }
        return true;
    }

    private enum UPDATE {

        IMPOSSIBLE, CONTINUE, CONFIMED;
    }

    private UPDATE expressionUpdate(ArrayList<OR> ORs) {

        for (int i = 0; i < ORs.size() - 1; i++) {
            OR exp_a = ORs.get(i);
            for (int j = i + 1; j < ORs.size(); j++) {
                OR exp_b = ORs.get(j);

                OR newOR = null;

                if (exp_a.A.equals(exp_b.A.inverse)) {
                    newOR = new OR(exp_a.B, exp_b.B);
                } else if (exp_a.A.equals(exp_b.B.inverse)) {
                    newOR = new OR(exp_a.B, exp_b.A);
                } else if (exp_a.B.equals(exp_b.A.inverse)) {
                    newOR = new OR(exp_a.A, exp_b.B);
                } else if (exp_a.B.equals(exp_b.B.inverse)) {
                    newOR = new OR(exp_a.A, exp_b.A);
                }

                if (newOR != null) {
                    if (!ORs.contains(newOR)) {
                        if (newOR.A.equals(newOR.B)) {
                            newOR.A.isForced = true;
                            if (newOR.A.inverse.isForced == true) {
                                return UPDATE.IMPOSSIBLE;
                            }
                        }
                        if (print) {
                            System.out.println(" NEW RULE : ");
                            newOR.print();
                            System.out.println(" FROM : ");
                            exp_a.print();
                            exp_b.print();
                            System.out.println();
                        }
                        ORs.add(newOR);
                        return UPDATE.CONTINUE;
                    }
                }
            }

        }

        return UPDATE.CONFIMED;
    }

    enum VAR_STATE {

        NONE, Negated, Real;
    }

    class Var implements Comparable<Var> {

        DataStructure.Label l;
        final public int x;
        final public int y;
        final POS left;
        final POS right;
        VAR_STATE state = VAR_STATE.NONE;

        public Var(DataStructure.Label l) {
            this.l = l;
            x = l.xBase;
            y = l.yBase;
            left = new POS(this, true);
            right = new POS(this, false);
            left.setInverse(right);
            right.setInverse(left);
        }

        public void setToOutput() {
            if (state == VAR_STATE.Real) {
                l.setPosition(LabelPosition.TopRight);
            } else {
                l.setPosition(LabelPosition.TopLeft);
            }
        }

        @Override
        public int compareTo(Var other) {
            if (x > other.x) {
                return 1;
            } else if (x < other.x) {
                return -1;
            } else {
                return 0;
            }
        }

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof Var)) {
                return false;
            }
            Var other = (Var) obj;
            return other.x == x && other.y == y;
        }

        public void updateBounds() {
            left.reconstructAndReset();
            right.reconstructAndReset();
        }

        public void pushRelations(ArrayList<OR> ORs) {
            left.pushRelations(ORs);
            right.pushRelations(ORs);
        }

        public boolean tryChoose() {
            // if it doesnt have a chose yet

            if (state == VAR_STATE.NONE) {
                // try left
                state = VAR_STATE.Negated;
                if (!right.tryNotChoose()) {
                    //  we could not go left
                    // auto go to right try
                } else {
                    // we could go left, end
                    return true;
                }
            }
            // if we didnt choose left, try right
            if (state != VAR_STATE.Real) {
                // try right
                state = VAR_STATE.Real;
                if (!left.tryNotChoose()) {
                    // if we could not go right
                    // auto go to end                  
                } else {
                    // we could go left, end                  
                    return true;
                }
            }
            System.out.println(" AA A ");
            state = VAR_STATE.NONE;
            return false;

            // we cannot choose in this situation
            // make pending
        }
    }

    class POS {

        public Var v;
        public POS inverse;
        public boolean negated;
        public boolean isForced = false;

        public ArrayList<POS> myRelations = new ArrayList();

        public boolean tryNotChoose() {

            for (POS rel : myRelations) {
                if (rel.isFalse()) {
                    return false;
                }
            }
            return true;
        }

        public boolean isFalse() {
            if (negated) {
                return v.state == VAR_STATE.Real;
            } else {
                return v.state == VAR_STATE.Negated;
            }
        }

        public void pushRelations(ArrayList<OR> ORs) {
            for (OR rel : ORs) {
                if (rel.A.equals(this)) {
                    myRelations.add(rel.B);
                } else if (rel.B.equals(this)) {
                    myRelations.add(rel.A);
                }
            }
        }

        public POS(Var v, boolean negated) {
            this.v = v;
            this.negated = negated;
        }

        public void setInverse(POS inv) {
            inverse = inv;
        }

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof POS)) {
                return false;
            }
            POS other = (POS) obj;
            return other.v.equals(v) && other.negated == negated;
        }

        public boolean doesIntersect(POS other) {
            return (left < other.right && right > other.left) && (bottom < other.top && top > other.bottom);
        }

        public boolean containsInclusive(POS other) {
            return (left < other.v.x && right > other.v.x && bottom <= other.v.y && top > other.v.y);
        }

        private float left;
        private float right;
        private float top;
        private float bottom;

        public void reconstructAndReset() {
            top = v.y + height;
            bottom = v.y;
            right = v.x + (negated ? 0 : height * ratio);
            left = right - height * ratio;
        }

        public String toString() {
            return (negated ? "-" : "") + "[" + v.x + ", " + v.y + "]";
        }
    }

    class OR {

        public POS A;
        public POS B;

        public OR(POS A, POS B) {
            this.A = A;
            this.B = B;
        }

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof OR)) {
                return false;
            }
            OR other = (OR) obj;
            return (A.equals(other.A) && B.equals(other.B)) || (A.equals(other.B) && B.equals(other.A));
        }

        public void print() {
            System.out.println(A.toString() + "  OR  " + B.toString());
        }
    }

}
