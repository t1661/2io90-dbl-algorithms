package DataStructure;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * The spacial structure of all the labels stored such that it is easy to 
 * identify intersections.
 * 
 * @author Bart
 */
public class LabelSpace {
       
    SpaceBox baseNode;
    
    public float maxX = 0;
    public float maxY = 0;
    
    /**
     * Get the base node of the Quad tree list.
     * @return the base node of the Quad tree list.
     */
    public SpaceBox getBaseNode()
    {
        return baseNode;
    }
    
    /**
     * Builds the Quad tree list.
     * @param lc The current placement and height of all labels.
     */
    public void build(LabelsContainer lc)
    {
        for(Iterator<Label> iter = lc.labelList.iterator(); iter.hasNext(); ) {
            Label l = iter.next();
            if (l.top() > maxX) maxX = l.top();
            if (l.right() > maxY) maxY = l.right();
        }
        
        baseNode = new SpaceBox(0, null, maxX / 2.0f, maxY / 2.0f, maxX / 2.0f);
        
        for(Iterator<Label> iter = lc.labelList.iterator(); iter.hasNext(); ) {
            Label l = iter.next();
            baseNode.placeRecursive(l);
        }
    }
    
    /**
     * Get a list of all intersecting labels.
     * @param l The label for which we need to find all intersections.
     * @return an ArrayList of all intersecting labels.
     */
    public ArrayList<Label> getIntersectingLabels(Label l)
    {
        ArrayList<Label> iList = new ArrayList();
        baseNode.intersectListRecursive(l, iList);
        return iList;
    }
}