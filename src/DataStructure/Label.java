package DataStructure;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Represents each label on the map.
 * 
 * @author bart
 */
public class Label {

    /**
     * Backup of the label. Used to modify the original without losing 
     * information.
     */
    private class Backup {
        public boolean isTop;
        public float shift;

        public Backup(boolean isTop, float shift) {
            this.isTop = isTop;
            this.shift = shift;
        }
    }

    // The label container to which this label is bound to.
    private final LabelsContainer boundContainer;
    
    // The node to which this label belongsin the Quad tree structure.
    private SpaceBox box;
    
    // Available label positions for this label.
    public PlacementAvailability PA = new PlacementAvailability();
    
    // Atributes of this label.
    public HashMap<Class<?>, Object> Attributes = new HashMap();
    
    // Label backups of this labels.
    private final HashMap<Integer, Backup> backups = new HashMap();
    
    /**
     * Set the node to which this label belongs to in the quad tree structure.
     * @param b the node to which the label must belong to.
     */
    public void setBox(SpaceBox b) {
        box = b;
    }

    // coordinates of the parent point feature.
    public final int xBase;
    public final int yBase;

    // the edges of the label.
    private float left;
    private float bottom;
    private float top;
    private float right;

    // isTop = true if the label is on top of its parent point-feature.
    private boolean isTop = true;
    
    /**
     * The displacement of the label along it bottom edge relative to its
     * parent point feature.
     */
    private float shift = 0;

    // hasIntersectForvisual = true if it is intersecting with any other label.
    public boolean hasIntersectForvisual = false;

    public Label(int x, int y, LabelsContainer lc) {
        this.boundContainer = lc;
        this.xBase = x;
        this.yBase = y;
        recalculateCorners();
    }
    
    // Getter method of the x-coordinate of the its left edge.
    public float left() {
        return left;
    }

    // Getter method of the x-coordinate of the its right edge.
    public float right() {
        return right;
    }

    // Getter method of the y-coordinate of the its top edge.
    public float top() {
        return top;
    }

    // Getter method of the y-coordinate of the its bottom edge.
    public float bottom() {
        return bottom;
    }

    /**
     * Gets the list of all labels that intersect with this one.
     * @return an ArrayList of all labels that intersect.
     */
    public ArrayList<Label> intersectList() {
        ArrayList<Label> iList = new ArrayList<>();
        if (!box.isBaseNode()) {
            box.getParent().intersectListUp(this, iList);
        }
        box.intersectListRecursive(this, iList);
        return iList;
    }

    /**
     * Check for intersections with any other label.
     * @return the first intersecting label otherwise null if there are no
     * intersections.
     */
    public Label hasIntersection() {
        if (!box.isBaseNode()) {
            Label interSectlabel = box.getParent().doesIntersectUp(this);
            if (interSectlabel != null) {
                return interSectlabel;
            }
        }

        Label hasIntersect = box.doesIntersectRecursive(this);
        return hasIntersect;
    }

    /**
     * Set the shift of this label over its parent point and recalculate the
     * corners of this label.
     * @param shift the new shift.
     */
    public void setShift(float shift) {
        this.shift = shift;
        recalculateCorners();
    }

    /**
     * Getter method for this label's current shift.
     * @return the shift of the label over its parent point.
     */
    public float getShift() {
        return shift;
    }

    /**
     * Set the new position of this label around its parent point and 
     * recalculate the corners.
     * @param lp the new label position.
     */
    public void setPosition(LabelPosition lp) {
        isTop = lp.isTop;
        shift = lp.shift;

        recalculateCorners();
    }

    /**
     * Gets the last height for which this label did not intersect anymore.
     * @return the last height for which this label did not intersect anymore.
     */
    public float getMaxHeightCurrent() {
        ArrayList<Label> iList = intersectList();
        float height = boundContainer.getHeight();
        for (Label l : iList) {
            float hBuffer = getSnapDown(l);
            if (hBuffer < height) {
                height = hBuffer;
            }
        }

        return height;
    }

    /**
     * Check the area upto which this label can grow with respect to the given
     * label.
     * @param l the label with respect to which we want to measure the are of
     * possible growth.
     * @return the distance upto which the width and height of this label can 
     * grow.
     */
    private float getSnapDown(Label l) {
        double xBasedH = 0;
        double yBasedH = 0;

        // X
        {
            double areaX;
            double xGrow;
            // determine area:
            if (left > l.left) {
                areaX = l.right - left;
            } else {
                areaX = right - l.left;
            }
            // determine height x based
            if (xBase > l.xBase) {
                xGrow = 1 - shift + l.shift;
            } else if (xBase < l.xBase) {
                xGrow = shift + 1 - l.shift;
            } else {
                xGrow = 0;
            }
            xBasedH = boundContainer.getHeight() - areaX / (xGrow * boundContainer.ratio);
        }

        // Y
        {
            double areaY;
            double yGrow;
            // determine area:
            if (bottom > l.bottom) {
                areaY = l.top - bottom;
            } else {
                areaY = top - l.bottom;
            }
            // determine height x based
            if (yBase > l.yBase) {
                yGrow = (isTop ? 0 : 1) + (l.isTop ? 1 : 0);
            } else if (yBase < l.yBase) {
                yGrow = (isTop ? 1 : 0) + (l.isTop ? 0 : 1);
            } else {
                yGrow = 0;
            }
            yBasedH = boundContainer.getHeight() - areaY / yGrow;
        }
        return (float) (xBasedH > yBasedH ? xBasedH : yBasedH);
    }

    /**
     * Check the area upto which this label must shrink if it overlaps with
     * the given label. Only used when the 2 labels overlap.
     * @param l the overlapping label with respect to which we measure the area
     * upto which we must reduce to prevent overlap.
     * @return the distance upto which the label width and height must be 
     * reduced to remove overlap.
     */
    private float getAntiOverlapHeight(Label l) {

        float xbasedH;
        float ybasedH;
        float xOffsetWeighted = -1;
        float yOffsetWeighted = -1;

        // x based:
        if (left != l.left) {
            float comboEffectX;
            float xArea;
            if (left > l.left) {
                xArea = l.right - left;
                comboEffectX = shift + 1 - l.shift;
            } else { // if (l.left > left)
                xArea = right - l.left;
                comboEffectX = 1 - shift + l.shift;
            }

            if (comboEffectX > 0) {
                xOffsetWeighted = xArea / comboEffectX;
                xbasedH = boundContainer.getHeight() - xOffsetWeighted / boundContainer.ratio;
                if (xbasedH < 0.1) {
                    System.out.println("X   H: " + boundContainer.getHeight() + "  weight: " + comboEffectX);
                }
            } else {
                xbasedH = xArea / (2.0f * boundContainer.ratio);
            }
        } else {
            xbasedH = 0;
        }

        // y based:
        if (bottom != l.bottom) {
            float comboEffectY;
            float yArea;
            if (bottom > l.bottom) {
                yArea = l.top - bottom;
                comboEffectY = (isTop ? 1 : 0) + (l.isTop ? 0 : 1);
            } else { // if (l.bottom > bottom)
                comboEffectY = (isTop ? 0 : 1) + (l.isTop ? 1 : 0);
                yArea = top - l.bottom;
            }
            if (comboEffectY > 0) {
                yOffsetWeighted = yArea / comboEffectY;
                ybasedH = boundContainer.getHeight() - yOffsetWeighted;
            } else {
                ybasedH = yArea / 2.0f;
            }
        } else {
            ybasedH = 0;
        }

        if (xbasedH == 0) {
            return ybasedH;
        } else if (ybasedH == 0) {
            return xbasedH;
        } else {
            return (xOffsetWeighted > yOffsetWeighted) ? xbasedH : ybasedH;
        }
    }
    
    /**
     * Get the current position of the label.= around its parent point.
     * @return the LabelPosition of this label.
     */
    public LabelPosition getPosition() {
        if (shift == 0) {
            if (isTop) {
                return LabelPosition.TopLeft;
            } else {
                return LabelPosition.BottomLeft;
            }
        } else if (shift == 1) {
            if (isTop) {
                return LabelPosition.TopRight;
            } else {
                return LabelPosition.BottomRight;
            }
        } else {
            System.out.println(" MISTAKE rgzd5 Label.java ");
            return LabelPosition.SLIDING;
        }
    }

    /**
     * Check for intersection with the given label.
     * @param l the label to check intersection with.
     * @return true if it intersects.
     */
    public boolean doesIntersectWith(Label l) {
        return (left < l.right && right > l.left) && (bottom < l.top && top > l.bottom);
    }

    /**
     * Get the list of possible positions around the parent point.
     * @param l The label with which it checks intersection in each position
     * of the given label.
     * @return a list of booleans which represent the 4 corners. For each corner,
     * if it is true, then that corner is a possible position.
     */
    public boolean[] positionsPossible(Label l) {
        boolean[] result = new boolean[4];

        result[LabelPosition.TopRight.id]
                = !(xBase < l.xBase
                && yBase < l.yBase
                && xBase + boundContainer.getWidth() > l.xBase
                && yBase + boundContainer.getHeight() > l.yBase);

        result[LabelPosition.TopLeft.id]
                = !(xBase - boundContainer.getWidth() < l.xBase
                && yBase < l.yBase
                && xBase > l.xBase
                && yBase + boundContainer.getHeight() > l.yBase);
        result[LabelPosition.BottomRight.id]
                = !(xBase < l.xBase
                && yBase - boundContainer.getHeight() < l.yBase
                && xBase + boundContainer.getWidth() > l.xBase
                && yBase > l.yBase);

        result[LabelPosition.BottomLeft.id]
                = !(xBase - boundContainer.getWidth() < l.xBase
                && yBase - boundContainer.getHeight() < l.yBase
                && xBase > l.xBase
                && yBase > l.yBase);

        return result;
    }

    /**
     * Checks for overlap between this label and a list of all possibly 
     * intersecting labels.
     * @return true if it intersects with any one label.
     */
    public boolean overlapsAlabelBase() {
        ArrayList<Label> rList = getPointsInRange();
        for (Label l : rList) {
            if (right > l.xBase && left < l.xBase && top > yBase && bottom < yBase) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the labels that possibly overlap.
     * @return list of parent points within range of this labels parent point.
     */
    public ArrayList<Label> getPointsInRange() {
        ArrayList<Label> rList = new ArrayList();

        float r2 = 2 * (boundContainer.getWidth() * boundContainer.getWidth() + boundContainer.getHeight() * boundContainer.getHeight());

        boundContainer.getBaseNode().labelPointsInRadius(this, rList, r2);

        return rList;
    }

    /**
     * Gets the distance (squared) between the parent point of this label and 
     * the given label. This is squared because root is slow and we only need 
     * relative values.
     * @param l the label to which we need to determine the distance to.
     * @return the distance squared between the parent point of this label and 
     * the given label.
     */
    public float r2DistTo(Label l) {
        float dx = l.xBase - xBase;
        float dy = l.yBase - yBase;
        return dx * dx + dy * dy;
    }

    /**
     * Recalculate the corners of this label.
     */
    public void recalculateCorners() {
        top = yBase + (isTop ? boundContainer.getHeight() : 0);
        bottom = top - boundContainer.getHeight();
        right = xBase + boundContainer.getHeight() * boundContainer.ratio * shift;
        left = right - boundContainer.getHeight() * boundContainer.ratio;

        if (box != null) {
            box.moved(this, true);
        }
    }

   /**
    * Add the current instance of this label into its backup.
    * @param ID the unique with which we can identify the new backup.
    */
    public void setThisAsBackup(int ID) {
        backups.put(ID, new Backup(isTop, shift));
    }

    /**
     * Restore the instance of the label present in the backup into the current
     * instance of the label.
     * @param ID the unique id of the backup to restore.
     */
    public void restoreBackup(int ID) {
        Backup b;
        if ((b = backups.get(ID)) != null) {
            shift = b.shift;
            isTop = b.isTop;
            recalculateCorners();
        }
    }

    /**
     * Delete a particular of the label.
     * @param ID the unique id of the backup to delete.
     */
    public void removeBackup(int ID) {
        backups.remove(ID);
    }

    /**
     * No idea why it is here. May the force be with you.
     * @param l2 a label with some force.
     * @return the force of the entire labelContainer to which the given label
     * is bound.
     */
    public LabelsContainer.Force forceFrom(Label l2) {
        float dx = l2.xBase - xBase;
        float dy = l2.yBase - yBase;

        float dxAbs = dx >= 0 ? dx : -dx;
        float dyAbs = dy >= 0 ? dy : -dy;

        LabelsContainer.Force f = boundContainer.new Force(dx * dxAbs, dy * dyAbs);
        return f;
    }

    /**
     * A copy function to copy the instance of this label into another.
     * @param newShift the new shift of the label we are returning.
     * @param isTopNew To the set a new isTop for the returning label.
     * @return a copy of the instance of this label with a new shift and isTop.
     */
    public Label simpleCopy(float newShift, boolean isTopNew) {
        Label result = new Label(xBase, yBase, boundContainer);
        result.shift = newShift;
        result.isTop = isTopNew;
        result.box = box;
        boundContainer.labelList.add(result);
        result.recalculateCorners();

        return result;
    }

    /**
     * Delete the instance of this label from its bound label container and from
     * the quad tree list.
     */
    public void close() {
        box.labels.remove(this);
        boundContainer.labelList.remove(this);
    }
    
    /**Checks if the given label has the same parent point as this label.
     * @param l the label to check.
     * @return true if the have the same parent position.
     */
    public boolean samePos(Label l) {
        return (l.xBase == xBase && l.yBase == yBase);
    }

}
