package DataStructure;

/**
 * Represents the label positions relative to their parent point feature.
 * @author Bart
 */
public enum LabelPosition {
    TopLeft(0,0,true),
    TopRight(1,1,true),
    BottomLeft(2,0,false),
    BottomRight(3,1,false),
    SLIDING(5,0,true);
    
    // Used to uniquely identify each position.
    final public int id;
    
    /**
     * The displacement of the label along it bottom edge relative to its
     * parent node.
     */
    final public float shift;
    
    // Used to identify if the label is on the top or bottom of its parent node.
    final public boolean isTop;
    
    LabelPosition(int id, float shift, boolean isTop)
    {
        this.id = id;
        this.shift = shift;
        this.isTop = isTop;
    }
}
