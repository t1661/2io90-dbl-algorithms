/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;

/**
 * Represents the placement model used for the map labeling.
 * 
 * @author Bart
 */
public enum Placement {
    TwoPos, FourPos, Slider
}
