package DataStructure;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents a node in the Quad tree structure.
 * 
 * @author bart
 */
public class SpaceBox
{
    // The maximum depth upto which the nodes can be created.
    final int maxDepth = 20;
    
    // The depth of the current node. Its 0, if its the Base node.
    int depth;
    
    // The list of labels present in the current node.
    ArrayList<Label> labels = new ArrayList<>();
    
    // The parent of the current node.
    SpaceBox parent;
    
    /**
     * The Left-Top, Right-Top, Left-Bottom, Right-Bottom nodes relative to the
     * current node.
     */
    SpaceBox LT = null;
    SpaceBox RT = null;
    SpaceBox LB = null;
    SpaceBox RB = null;
    
    float centerHeight;
    float centerWidth;
    float halfWidth;
    
    public SpaceBox(int depth, SpaceBox parent, float centerHeight, float centerWidth, float halfWidth) {
        this.parent = parent;
        this.depth = depth;
        this.centerHeight = centerHeight;
        this.centerWidth = centerWidth;
        this.halfWidth = halfWidth;
    }
    
    /**
     * Gets the immediate parent of the current node. Returns null, if it is 
     * the base node.
     * @return the parent node.
     */
    public SpaceBox getParent()
    {
        return parent;
    }
    
    /**
     * To check if the current node is the base node.
     * @return true if is the parent of all nodes.
     */
    public boolean isBaseNode()
    {
        return parent == null;
    }
    
    /**
     * If label is from current node, delete it. Then move the label to the
     * correct child node.
     * @param l The label which need to be moved.
     * @param fromThis true if it from the current node otherwise false.
     */
    public void moved(Label l, boolean fromThis) {
        if (fromThis) labels.remove(l);
        
        if (depth > 0) {
            if (l.left() > centerWidth - halfWidth
            && l.right() < centerWidth + halfWidth
            && l.top() < centerHeight + halfWidth
            && l.bottom() > centerHeight - halfWidth)
            {
                placeRecursive(l);
            }
            else {
                parent.moved(l,false);
            }
        }
        else {
            placeRecursive(l);
        }
    }
    
    /**
     * Finds any label present in all parent nodes intersecting with the given
     * label.
     * @param l The label for which we need to find an intersecting label.
     * @return the intersecting label otherwise null if no intersecting label.
     */
    public Label doesIntersectUp(Label l)
    {
        for(Iterator<Label> iter = labels.iterator(); iter.hasNext(); ) {
            Label l2 = iter.next();
            if (l.doesIntersectWith(l2) && l != l2) return l2;
        }
        
        if (parent != null) {
            Label hasIntersect = parent.doesIntersectUp(l);
            if (hasIntersect != null) return hasIntersect;
        }
        
        return null;
    }
    
    /**
     * Find any label intersecting with the given label.
     * @param l The label we want to find any intersecting label.
     * @return the intersecting label otherwise null if no intersecting label.
     */
    public Label doesIntersectRecursive(Label l)
    {
        for(Iterator<Label> iter = labels.iterator(); iter.hasNext(); ) {
            Label l2 = iter.next();
            if (l.doesIntersectWith(l2) && l != l2) {
                return l2;
            }          
        }

        if (l.right() > centerWidth)
        {
            if (l.top() > centerHeight && RT != null) {
                Label hasIntersect = RT.doesIntersectRecursive(l);
                if (hasIntersect != null) return hasIntersect;
            }         
            if (l.bottom() < centerHeight && RB != null)
            {
                Label hasIntersect = RB.doesIntersectRecursive(l);
                if (hasIntersect != null) return hasIntersect;
            }
        }
        if (l.left()  < centerWidth)
        {
            if (l.top() > centerHeight && LT != null) {
                Label hasIntersect = LT.doesIntersectRecursive(l);
                if (hasIntersect != null) return hasIntersect;
            }
            if (l.bottom() < centerHeight && LB != null) {
                Label hasIntersect = LB.doesIntersectRecursive(l);
                if (hasIntersect != null) return hasIntersect;
            }       
        }
        
        return null;
    }
    
    /**
     * Find all the possibly intersecting labels present in parent nodes around 
     * the given label.
     * @param l The label for which we need to find all possibly intersecting
     * label.
     * @param iList The list we add the possibly intersecting labels to.
     */
    public void intersectListUp(Label l, ArrayList<Label> iList) {
        for(Iterator<Label> iter = labels.iterator(); iter.hasNext(); ) {
            Label l2 = iter.next();
            if (l.doesIntersectWith(l2) && l != l2) iList.add(l2);
        }
        
        if (parent != null) parent.intersectListUp(l, iList);
    }
    
    /**
     * Find all the possibly intersecting labels present in child nodes around 
     * the given label.
     * @param l The label for which we need to find all possibly intersecting
     * label.
     * @param iList The list we add the possibly intersecting labels to.
     */
    public void intersectListRecursive(Label l, ArrayList<Label> iList) 
    {
        for(Iterator<Label> iter = labels.iterator(); iter.hasNext(); ) {
            Label l2 = iter.next();
            if (l.doesIntersectWith(l2) && l != l2) {
                iList.add(l2);
            }
        }

        if (l.right() > centerWidth)
        {
            if (l.top() > centerHeight && RT != null) RT.intersectListRecursive(l,iList);         
            if (l.bottom() < centerHeight && RB != null) RB.intersectListRecursive(l,iList);            
        }
        if (l.left()  < centerWidth)
        {
            if (l.top() > centerHeight && LT != null) LT.intersectListRecursive(l,iList);         
            if (l.bottom() < centerHeight && LB != null) LB.intersectListRecursive(l,iList);           
        }
    }
    
    /**
     * FInds all the labels in the given radius around the current label.
     * @param l the label around which we need to find all labels within given
     * radius.
     * @param rList the list t which we add the the labels around the given label.
     * @param r2 The radius within which we need to identify surrounding labels.
     */
    public void labelPointsInRadius(Label l, ArrayList<Label> rList, float r2)
    {
        for(Iterator<Label> iter = labels.iterator(); iter.hasNext(); ) {
            Label l2 = iter.next();
            if (l.r2DistTo(l2) <= r2 && l != l2) {
                rList.add(l2);
            }          
        }
        
        if (l.xBase + r2 > centerWidth)
        {
            if (l.yBase + r2 > centerHeight && RT != null) RT.labelPointsInRadius(l,rList,r2);         
            if (l.yBase - r2 < centerHeight && RB != null) RB.labelPointsInRadius(l,rList,r2);            
        }
        if (l.xBase - r2  < centerWidth)
        {
            if (l.yBase + r2 > centerHeight && LT != null) LT.labelPointsInRadius(l,rList,r2);         
            if (l.yBase - r2 < centerHeight && LB != null) LB.labelPointsInRadius(l,rList,r2);           
        }
    }
    
    /**
     * Place the given label into one of the child nodes. It recursively calls
     * this method to find the correct child node to place the label in.
     * @param l the label to placed.
     */
    public void placeRecursive(Label l) 
    {
        boolean placeHere = true;
        
        if (depth <= maxDepth) {      
            if (l.bottom() > centerHeight) {
                if (l.left() > centerWidth) {
                    if (RT == null) RT = new SpaceBox(depth+1, this, centerHeight + halfWidth / 2, centerWidth + halfWidth / 2, halfWidth / 2);
                    RT.placeRecursive(l);
                    placeHere = false;
                }
                else if (l.right() < centerWidth) {
                    if (LT == null) LT = new SpaceBox(depth+1, this, centerHeight + halfWidth / 2, centerWidth - halfWidth / 2, halfWidth / 2);
                    LT.placeRecursive(l);
                    placeHere = false;
                }
            }
            else if (l.top() < centerHeight) {
                if (l.left() > centerWidth) {
                    if (RB == null) RB = new SpaceBox(depth+1, this, centerHeight - halfWidth / 2, centerWidth + halfWidth / 2, halfWidth / 2);
                    RB.placeRecursive(l);
                    placeHere = false;
                }
                else if (l.right() < centerWidth) {
                    if (LB == null) LB = new SpaceBox(depth+1, this, centerHeight - halfWidth / 2, centerWidth - halfWidth / 2, halfWidth / 2);
                    LB.placeRecursive(l);
                    placeHere = false;
                }
            }
        }
        
        if (placeHere) 
        {
            putLabelHere(l);
        }
    }
    
    /**
     * Places the given label in the current node of the spacial structure.
     * @param l the labels to placed.
     */
    private void putLabelHere(Label l) {
        labels.add(l);
        l.setBox(this);
    }  
   
}
