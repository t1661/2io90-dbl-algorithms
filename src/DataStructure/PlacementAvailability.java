/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructure;
import java.util.ArrayList;

/**
 * Represents the available label positions around the parent point.
 * 
 * @author Bart
 */
public class PlacementAvailability {
    
    // List of all available label positions.
    public ArrayList<LabelPosition> availables = new ArrayList();
    
    public PlacementAvailability()
    {
        reset();
    }
    
    /**
     * Reset the list of available positions around the parent point.
     */
    public void reset()
    {
        availables = new ArrayList();
        availables.add(LabelPosition.TopRight);
        availables.add(LabelPosition.TopLeft);
        availables.add(LabelPosition.BottomRight);
        availables.add(LabelPosition.BottomLeft);
    }
    
    /**
     * Sets the list of available label positions around the parent point when
     * the placement model used is 2p and 4p only.
     * @param possible the set of booleans which represent the possible positions
     * that need to be set.
     */
    public void pushImpossible(boolean[] possible)
    {
        for(LabelPosition LP : LabelPosition.values())
        {
            if (LP != LabelPosition.SLIDING)
            {
                if (!possible[LP.id]) {
                    availables.remove(LP);    
                }
            }
        }
    }
        
}