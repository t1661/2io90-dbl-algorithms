package AlgorithmSlider1;

import group5.ReadInput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import Algorithm1.AlgoOne;
import DataStructure.Label;
import DataStructure.LabelsContainer;
import DataStructure.Placement;

public class AlgoSliderOne {
	
	private LabelsContainer lc;
	public static boolean show = true;
	
	@SuppressWarnings("unchecked")
	public AlgoSliderOne(LabelsContainer lc) {
		this.lc = lc;
		
		// sort the label list from left to right
		Collections.sort(this.lc.labelList, new Comparator<Label>() {

			@Override
			public int compare(Label label1, Label label2) {
				return label1.xBase - label2.xBase;
			}
		});
	}
	
	public void execute() {
		float delta = 0.002f;
		float height = 1f;
		float heightLow = 0f;
		float heightHigh = 100f;
		boolean expFase = true;
		boolean success;
		
		int backup = lc.createBackup();
		
		while (true) {
			lc.setHeight(height);
			
			// boolean value: whether or not the label has been 'placed' already
			for (Label label : lc.labelList) {
				label.Attributes.put(Boolean.class, false);
				label.setShift(0);
			}
			
			
			success = placeLabels();
			
			if (success) {
				backup = lc.createBackup();
				heightLow = height;
				if (expFase) {
					height *= 2;
				} else {
					height = (heightLow + heightHigh)/2.0f;
				}
			} else {
				heightHigh = height;
				if (expFase) {
					expFase = false;
				}
				height = (heightLow + heightHigh)/2.0f;
			}

			show();
			
			if (heightHigh - heightLow < delta) {
				lc.restoreBackup(backup, true);
				show();
				return;
			}
		}
	}
	
    public void show() {
    	if (!AlgoSliderOne.show) { return; }
        lc.setAllIntersections();

        Graphics.Display.setContent(lc);
    }

	private boolean placeLabels() {
		
		float leftBound;
		ArrayList<Label> intersectList;
		
		for (Label label : lc.labelList) {
			
			leftBound = label.left();			
			intersectList = label.intersectList();
			
			for (Label intersectLabel : intersectList) {
				if ((boolean) intersectLabel.Attributes.get(Boolean.class)) {
					leftBound = Math.max(leftBound, intersectLabel.right());
				}
			}
			
			if (leftBound <= label.right()) {
				// shift = (leftBound - label.left) / width
				label.setShift((leftBound - label.left())/(label.right() - label.left()));
			} else {
				// impossible to place this label
				return false;
			}
			
			label.Attributes.put(Boolean.class, true);
		}
		
		return true;
	}

	public static void testAlgo() {
		
        Scanner sc = new Scanner(System.in);
        DataStructure.LabelsContainer newData = null;
        
        newData = ReadInput.loadRandom(100, Placement.Slider, 1.25f, 5.0f);
        AlgoSliderOne algoSliderOne = new AlgoSliderOne(newData);
        algoSliderOne.execute();
        
	}
}
